#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2021-01-30 Sat 03:00 PM
## Comment : Run ANN predict
##
## ------------------------------
import sys, os, re, json, math
import pandas as pd
import numpy as np

from hydrodiy.io import iutils

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------
version = 4
month = 1
xvyear = 'full'

#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
source_file = os.path.abspath(__file__)
froot = os.path.join(os.path.dirname(source_file), '..', '..')

fdata = os.path.join(froot, 'data')

fout = os.path.join(froot, 'outputs', 'fit')

fdeploy = os.path.join(froot, 'outputs', 'deploy')

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
LOGGER = iutils.get_logger(basename)

#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------

model_path = os.path.join(fout, f'ml_streamflow_month{month:02d}_xv{xvyear}')

# Test prediction cli
cmd = f'saved_model_cli run --dir {model_path} --tag_set serve '+ \
                 '--signature_def serving_default '+ \
                 f'--inputs input={fw}'
os.system(cmd)


LOGGER.info('Process completed')

