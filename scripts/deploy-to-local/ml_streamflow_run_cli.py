#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2021-01-30 Sat 03:00 PM
## Comment : Run ANN predict
##
## ------------------------------
import sys, os, re, json, math
import time, argparse
from itertools import product as prod
import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow import keras

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

from hydrodiy.io import csv, iutils
from hydrodiy.plot import putils

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------
version = 4
month = 1
xvyear = 'full'

#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
source_file = os.path.abspath(__file__)
froot = os.path.join(os.path.dirname(source_file), '..', '..')

fdata = os.path.join(froot, 'data')

fout = os.path.join(froot, 'outputs', 'fit')

fdeploy = os.path.join(froot, 'outputs', 'deploy')
os.makedirs(fdeploy, exist_ok=True)

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
LOGGER = iutils.get_logger(basename)

#----------------------------------------------------------------------
# Get data
#----------------------------------------------------------------------
fd = os.path.join(fdata, f'streamflow_data_version{version}.csv')
data, _ = csv.read_csv(fd, index_col=0, parse_dates=True)
data = data.loc['1981':, :]

#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------

model_path = os.path.join(fout, f'ml_streamflow_month{month:02d}_xv{xvyear}')

# Load data
fd = os.path.join(model_path, f'training_config_month{month:02d}_xv{xvyear}.npz')
config = np.load(fd)
Wcols, Zcols, idx_valid, idx_train = config['Wcols'], config['Zcols'], \
                                    config['idx_train'], config['idx_valid']
dates = data.index[idx_valid]
Wvalid = data.loc[idx_valid, Wcols].values.astype(np.float32)

Wtest = Wvalid[:2]
fw = os.path.join(fdeploy, 'inputs.npy')
np.save(fw, Wtest)

# Test prediction cli
cmd = f'saved_model_cli run --dir {model_path} --tag_set serve '+ \
                 '--signature_def serving_default '+ \
                 f'--inputs input={fw}'
os.system(cmd)


LOGGER.info('Process completed')

