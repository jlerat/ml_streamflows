import joblib
import numpy as np
import os
import tensorflow as tf
from tensorflow.keras.models import load_model
from tensorflow.keras import layers
import pickle

# from inference_schema.schema_decorators import input_schema, output_schema
# from inference_schema.parameter_types.numpy_parameter_type import NumpyParameterType


# The init() method is called once, when the web service starts up.
#
# Typically you would deserialize the model file, as shown here using joblib,
# and store it in a global variable so your run() method can access it later.



def init(local=False):
    global model

    if local:
        fsource = os.path.dirname(os.path.abspath(__file__))
        model_dir = os.path.join(fsource, '..', '..', 'outputs', 'fit', 'ml_streamflow_month01_xvsplit')
    else:
        model_dir = os.environ['AZUREML_MODEL_DIR']
  
    # read pickle files
    #pickle_path = os.path.join(model_dir, 'streamflow_logs.pkl')

    #with open(pickle_path, 'rb') as file:
    #    # dump information to that file
    #    logX,logX_mean,logX_std = pickle.load(file)    
    
    # The AZUREML_MODEL_DIR environment variable indicates
    # a directory containing the model file you registered.
    #model_filename = 'ml_streamlofw_month01_xvsplit.h5'
    #model_path = os.path.join(model_dir, model_filename)

    #model = load_model(model_path, custom_objects={'tf': tf,'logX_mean': logX_mean} )
    #model = load_model(model_path, custom_objects={'tf': tf})
    model = tf.saved_model.load(model_dir)


# The run() method is called each time a request is made to the scoring API.
#
# Shown here are the optional input_schema and output_schema decorators
# from the inference-schema pip package. Using these decorators on your
# run() method parses and validates the incoming payload against
# the example input you provide here. This will also generate a Swagger
# API document for your web service.
# @input_schema('data', NumpyParameterType(np.ones(1380)))
# @output_schema(NumpyParameterType(np.ones(644)))
def run(data):
    # Use the model object loaded by init().
    #result = model.predict(data)
    _, result = model(data, training=False)
    
    # You can return any JSON-serializable object.
    return result.numpy().tolist()
