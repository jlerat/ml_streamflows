
import numpy as np
from score import init, run

# Random rainfall and streamflow inputs
inputs = [
    np.random.uniform(0, 1, size=(5, 129)).astype(np.float32), \
    np.random.uniform(0, 1, size=(5, 129)).astype(np.float32)
]

# Initialise model
init(local=True)

# Run model
preds = run(inputs)
print(preds)
