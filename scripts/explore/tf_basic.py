#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2020-11-30 12:55:30.750917
## Comment : Try basic TF code
##
## ------------------------------
import sys, os, re, json, math

import numpy as np
import tensorflow as tf

from hydrodiy.io import csv, iutils

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------

#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
source_file = os.path.abspath(__file__)
froot = os.path.join(os.path.dirname(source_file), '..', '..')


fout = os.path.join(froot, 'outputs', 'basics')
os.makedirs(fout, exist_ok=True)

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
LOGGER = iutils.get_logger(basename)

#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------

rank_0_tensor = tf.constant(4)
print(rank_0_tensor)

rank_1_tensor = tf.constant([2.0, 3.0, 4.0])
print(rank_1_tensor)

rank_2_tensor = tf.constant([[1, 2],
                             [3, 4],
                             [5, 6]], dtype=tf.float16)
print(rank_2_tensor)

a = tf.constant([[1, 2],
                 [3, 4]])
b = tf.constant([[1, 1],
                 [1, 1]]) # Could have also said `tf.ones([2,2])`

print(f'Add : {tf.add(a, b)}\n')
print(f'Multiply: {tf.multiply(a, b)}\n')
print(f'Matmul: {tf.matmul(a, b)}\n')

print(f'Add 2: {a+b}\n')
print(f'Multiply 2: {a*b}\n')
print(f'Matmul 2: {a@b}\n')


a = tf.cast(tf.random.uniform((3, 4))*10, tf.int32)
print(tf.reshape(a, 12))


LOGGER.info('Process completed')

