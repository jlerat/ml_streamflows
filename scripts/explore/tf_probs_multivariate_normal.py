#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2020-11-30 12:55:30.750917
## Comment : Try basic TF probability code
##
## ------------------------------
import sys, os, re, json, math

import numpy as np
import pandas as pd
from scipy.linalg import toeplitz

import tensorflow.compat.v2 as tf
tf.enable_v2_behavior()
import tensorflow_probability as tfp

import matplotlib.pyplot as plt

from netCDF4 import Dataset

from hydrodiy.io import csv, iutils
from hydrodiy.plot import boxplot
import time

tfd = tfp.distributions
tfb = tfp.bijectors

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------

#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
source_file = os.path.abspath(__file__)
froot = os.path.join(os.path.dirname(source_file), '..', '..')


fout = os.path.join(froot, 'outputs', 'basics')
os.makedirs(fout, exist_ok=True)

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
LOGGER = iutils.get_logger(basename)

#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------

# Generate some data
nvar = 5
nobs = 500

rho = 0.8
k = rho**np.arange(nvar)
cov = toeplitz(k)
true_params = cov[np.triu_indices(nvar, 1)]

mu = np.zeros(nvar)
ys = np.random.multivariate_normal(mean=mu, cov=cov, size=nobs)
ys = ys.astype(np.float32)

# Config parameters
alpha = tf.constant(5.)
beta = tf.constant(5.)
mu = tf.zeros(nvar)

def get_cov(w):
    vect = tf.math.pow(w, tf.cast(tf.range(nvar), tf.float32))
    return tf.linalg.LinearOperatorToeplitz(vect, vect)

def untrans(wt):
    w = tf.tanh(wt)
    return w, tf.math.log(1-w**2)


def joint_log_prob(wt, y):
    # Transform parameters
    w, logjac = untrans(wt)

    # .. prior
    prior = tfd.Beta(alpha, beta)
    pprior = prior.log_prob(w)

    # .. likelihood
    cov = get_cov(w).to_dense()
    like = tfd.MultivariateNormalFullCovariance(loc=mu, \
                            covariance_matrix=cov)
    pplike = tf.reduce_sum(like.log_prob(y), axis=0)

    return (pprior+pplike+logjac)


# Create our unnormalized target density by currying x and y from the joint.
def unnormalized_posterior(w):
  return joint_log_prob(w, ys)


# Create an HMC TransitionKernel
hmc_kernel = tfp.mcmc.HamiltonianMonteCarlo(
  target_log_prob_fn=unnormalized_posterior,
  step_size=np.float64(.1),
  num_leapfrog_steps=2)

nut_kernel = tfp.mcmc.NoUTurnSampler(
  target_log_prob_fn=unnormalized_posterior,
  step_size=np.float64(0.2))


# We wrap sample_chain in tf.function, telling TF to precompile a reusable
# computation graph, which will dramatically improve performance.
@tf.function
def run_chain(initial_state, num_results=1000, num_burnin_steps=500):
  return tfp.mcmc.sample_chain(
    num_results=num_results,
    num_burnin_steps=num_burnin_steps,
    current_state=initial_state,
    kernel=nut_kernel,
    trace_fn=lambda current_state, kernel_results: kernel_results)

# try sampling
#nsamples = 500
#sampler = tfd.Normal(loc=np.float32(rho), scale=0.5)
#params = np.zeros(nsamples)
#logp = np.zeros(nsamples)
#for i in range(nsamples):
#    wt = sampler.sample()
#    #w, _ = untrans(wt)
#    #cov = get_cov(w)
#
#    logp[i] = unnormalized_posterior(wt).numpy()
#    w, logjac = untrans(wt)
#    params[i] = w.numpy()
#
#plt.close('all')
#fig, ax = plt.subplots()
#ax.plot(params, logp, 'o')
#ylim = ax.get_ylim()
#ax.vlines(rho, ylim[0], ylim[1])
#ax.set_ylim(ylim)
#plt.show()
#sys.exit()


initial_state = 0.
samples, kernel_results = run_chain(initial_state)
print("Acceptance rate:", kernel_results.is_accepted.numpy().mean())
sys.exit()



# Testing using netcdf4
#a = np.random.uniform(0, 1, size=(100, 10, 20))
#ncfile = os.path.join(fout, 'test.nc')
#names = ['t', 'x', 'y']
#sizes = [100, 20, 10]
#with Dataset(ncfile, 'w') as nc:
#    for name, size in zip(names, sizes):
#        nc.createDimension(name, size)
#        v = nc.createVariable(name, np.int32, (name))
#        v[:] = np.arange(size)
#
#
#    v = nc.createVariable('data', np.float32, ('t', 'y', 'x'))
#    v[:] = a
#
sys.exit()


# Trace plots
colors = ['b', 'g', 'r']
for i in range(3):
  plt.plot(samples[:, i], c=colors[i], alpha=.3)
  plt.hlines(true_w[i], 0, 1000, zorder=4, color=colors[i], label="$w_{}$".format(i))
plt.legend(loc='upper right')
plt.show()

# Histogram of samples
for i in range(3):
  sns.distplot(samples[:, i], color=colors[i])
ymax = plt.ylim()[1]
for i in range(3):
  plt.vlines(true_w[i], 0, ymax, color=colors[i])
plt.ylim(0, ymax)
plt.show()

sys.exit()

# Visualize the data set
plt.scatter(*xs, c=ys, s=100, linewidths=0)

grid = np.meshgrid(*([np.linspace(-1, 1, 100)] * 2))
xs_grid = np.stack(grid, axis=0)
fs_grid = f(xs_grid.reshape([nvar, -1]), true_w)
fs_grid = np.reshape(fs_grid, [100, 100])
plt.colorbar()
plt.contour(xs_grid[0, ...], xs_grid[1, ...], fs_grid, 20, linewidths=1)
plt.show()

LOGGER.info('Process completed')


