#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2020-11-30 12:55:30.750917
## Comment : Try basic TF code
##
## ------------------------------
import sys, os, re, json, math

import numpy as np
from numpy import fft
import tensorflow as tf
import pandas as pd

import matplotlib.pyplot as plt

from hydrodiy.io import csv, iutils
from hydrodiy.plot import boxplot
import time

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------
D0 = 0.2

nx, ny = 100, 80
nsamples = 10

dim = 4*nx*ny
sqdim = math.sqrt(dim)

nboot = 5

#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
source_file = os.path.abspath(__file__)
froot = os.path.join(os.path.dirname(source_file), '..', '..')


fout = os.path.join(froot, 'outputs', 'basics')
os.makedirs(fout, exist_ok=True)

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
LOGGER = iutils.get_logger(basename)

#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------

def sample(nx, ny, method, D0):
    if method == 'np':
        distx = np.linspace(0, 1, nx)
        disty = np.linspace(0, 1, ny)

        # Build first line of embedded covariance matrix
        # using a Gaussian kernel
        s = np.exp(-(distx[None, :]**2+disty[:, None]**2)/D0**2)

        s = np.column_stack([s, s[:, -1][:, None], np.fliplr(s[:, 1:])])
        flip = np.flipud(s)
        s = np.row_stack([s, flip[0][None, :], flip[:-1]])
        s = s.ravel()

        # Compute eigenvalue decomposition of S via FFT
        # thanks to circulant matrices properties
        stilde = fft.ifft2(s.reshape((2*ny, 2*nx))).flatten().real*dim
        stilde = np.maximum(stilde, 1e-10)

        s2 = fft.ifft2(stilde.reshape((2*ny, 2*nx))).flatten().real
        err = np.abs(s2-s).max()

        # Sample random normal deviates and convert to complex
        e = np.random.normal(size=(dim, nsamples))
        e = e[:, :nsamples//2]+1j*e[:, nsamples//2:]

        # Multiply by eigenvalues and reshape
        D = np.sqrt(stilde)
        et = D[:, None]*e
        et = et.reshape((2*ny, 2*nx, nsamples//2))

        # Apply FFT 2d
        ismp = fft.fft2(et, axes=(0, 1)).reshape((4*nx*ny, nsamples//2))/sqdim
        y = np.column_stack([ismp.real, ismp.imag])

    elif method == 'tf':
        distx = tf.expand_dims(tf.linspace(0, 1, nx), 0)
        disty = tf.expand_dims(tf.linspace(0, 1, ny), -1)

        # Build first line of embedded covariance matrix
        # using a Gaussian kernel
        s = tf.math.exp(-(distx**2+disty**2)/D0**2)

        sc = s[:, -1][:, None]
        sr = tf.reverse(s[:, 1:], [-1])
        s = tf.concat([s, sc, sr], axis=-1)
        flip = tf.reverse(s, [0])
        rc = flip[0][None, :]
        s = tf.concat([s, rc, flip[:-1]], axis=0)
        s = tf.cast(s, tf.complex128)

        # Compute eigenvalue decomposition of S via FFT
        # thanks to circulant matrices properties
        stilde = tf.signal.ifft2d(s)

        stilde = tf.reshape(tf.math.real(stilde), -1)*dim
        stilde = tf.maximum(stilde, 1e-10)

        s2 = tf.signal.ifft2d(tf.reshape(tf.cast(stilde, tf.complex128),\
                                (2*ny, 2*nx)))
        s2 = tf.reshape(tf.math.real(s2), -1)
        err = tf.math.abs(s2-tf.math.real(tf.reshape(s, -1)))

        # Sample random normal deviates and convert to complex
        e = tf.random.normal(shape=(nsamples, dim))
        e = tf.complex(e[:nsamples//2, :], e[nsamples//2:, :])

        # Multiply by eigenvalues and reshape
        D = tf.math.sqrt(stilde)

        et = e*tf.cast(D, tf.complex64)[None, :]
        et = tf.reshape(et, (nsamples//2, 2*ny, 2*nx))

        # Apply FFT 2d
        ismp = tf.signal.fft2d(et)
        ismp = tf.reshape(ismp, (nsamples//2, 4*nx*ny))/sqdim
        y = tf.concat([tf.math.real(ismp), tf.math.imag(ismp)], axis=0)

    return y, s, stilde, err


res = pd.DataFrame(columns=['np', 'tf'], index=np.arange(nboot))

for i in range(nboot):
    LOGGER.info(f'Step {i+1}/{nboot}')
    t0 = time.perf_counter()
    #snp = np.random.normal(size=(nx, ny, nval))
    ynp, snp, stildenp, errnp = sample(nx, ny, 'np', D0)
    res.loc[i, 'np'] = time.perf_counter()-t0

    t0 = time.perf_counter()
    #stf = tf.random.normal(shape=(nx, ny, nval))
    ytf, stf, stildetf, errtf = sample(nx, ny, 'tf', D0)
    res.loc[i, 'tf'] = time.perf_counter()-t0

plt.close('all')
bp = boxplot.Boxplot(res)
bp.draw()
plt.show()

LOGGER.info('Process completed')

