#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os, json
import pandas as pd
import numpy as np
from scipy.stats import spearmanr
import tensorflow as tf
from gridverif import gridmetrics

FROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..')
FDATA = os.path.join(FROOT, 'data')

def get_model_name(month, rainfall_forecasts, linear_regression, loss_func, \
                        training_dataset):
    return f'M{month:02d}_RF{str(rainfall_forecasts).lower()}'+\
            f'_LR{str(linear_regression).lower()}'+\
            f'_LO{loss_func.lower()}_TR{training_dataset}'

# ------ data transformation functions ---------------
def datatrans_np(w, lam_trans, eps_trans):
    with np.errstate(invalid='ignore'):
        return np.power(w+eps_trans, lam_trans,)


def datatrans_tf(w, xmean, lam_trans, eps_trans, zero_thresh):
    if lam_trans is None:
        return w

    iszero = (w>=0) & (w<zero_thresh)
    low = tf.math.pow(eps_trans+zero_thresh, lam_trans)*tf.ones_like(w)
    x = tf.where(iszero, low, tf.math.pow(w+eps_trans, lam_trans))
    x = tf.where(w<0, xmean, x)
    return x


def invdatatrans_tf(y, lam_trans, eps_trans):
    if lam_trans is None:
        return y

    with np.errstate(invalid='ignore'):
        return tf.math.pow(tf.maximum(y, 1e-6), 1./lam_trans)-eps_trans


def positive_tf(y):
    return tf.where(y<0, tf.zeros_like(y), y)

# ------ training, val and test data -----------------
def get_model_data(data, idx, pattern, lam_trans, eps_trans):
    ''' Extract data from pandas data frame '''
    # Raw inputs
    U = data.loc[idx, :].filter(regex=pattern, axis=1)
    Ucols = U.columns.tolist()
    U = U.values.astype(np.float32)

    # Transformed inputs
    if not lam_trans is None:
        V = datatrans_np(U, lam_trans, eps_trans)
    else:
        V = U.copy()

    return U, V, Ucols


def get_normalising_constant(V):
    ''' Compute mean and std '''
    #Vmean = tf.constant(np.nanmean(V, axis=0))
    #Vstd = tf.constant(np.nanstd(V, axis=0))
    Vmean = np.nanmean(V, axis=0)
    Vstd = np.nanstd(V, axis=0)
    return Vmean, Vstd


def get_WXZ(data, idxs, data_patterns, trans):
    out = {}
    for pkey, pattern in data_patterns.items():
        out[pkey] = {}
        for ikey, idx in idxs.items():
            U, V, Ucols = get_model_data(data, idx, pattern, \
                                trans[pkey]['lam'], trans[pkey]['eps'])
            Vmean, Vstd = get_normalising_constant(V)

            out[pkey][ikey] = {
                'raw': U, \
                'cols': Ucols, \
                'trans': V, \
                'trans_mean': Vmean, \
                'trans_std': Vstd, \
                'trans_norm': (V-Vmean[None, :])\
                                    /Vstd[None, :], \
                'nval': V.shape[0], \
                'nvar': V.shape[1]
            }

    return out

# ------------ Define loss functions ---------------------
def compute_err_nonan(obs, pred):
    err = obs-pred
    err = tf.sign(err)*tf.maximum(tf.abs(err), 1e-5)
    isnan = tf.math.is_nan(err)
    return tf.where(isnan, tf.zeros_like(err), err)

def lossmse(obs, pred):
    err_nonan = compute_err_nonan(obs, pred)
    sqerr = tf.square(err_nonan)
    loss = tf.sqrt(tf.math.reduce_mean(sqerr, axis=-1))
    return loss

# log Determinant of the error covariance matrix
# note that we have to compute the determinant via
# the product of eigenvalues to avoid floating point
# errors.
#
# See Box & Tiao, Eq 8.2.23 page 428 for theory
def lossbayes(obs, pred):
    err_nonan = compute_err_nonan(obs, pred)
    S = tf.matmul(tf.transpose(err_nonan), err_nonan)
    eig = tf.linalg.eig(S)[0]
    D = tf.cast(tf.reduce_sum(tf.math.log(eig)), tf.float32)

    return D


def loss_dummy(obs, pred):
    return 0. #tf.zeros_like(obs)


# --------- Performance metrics -------------------------
def perf_metrics(obs, sim):
    idx = ~np.isnan(obs)
    # Spearman rank correlation
    rsp = spearmanr(obs[idx], sim[idx])[0]

    # NSE
    err = sim[idx]-obs[idx]
    errc = np.mean(obs[idx])-obs[idx]
    nse = 1-np.sum(err**2)/np.sum(errc**2)

    # MAE score
    maes = 1-np.sum(np.abs(err))/np.sum(np.abs(errc))

    return rsp, nse, maes


def verif_scores(obs, ens, timedim, ensdim):
    with np.errstate(invalid='ignore'):
        # Compute CRPS
        crps_u = gridmetrics.crps_uncertainty(obs, timedim=timedim).squeeze()
        crps = gridmetrics.crps(obs, ens, timedim=timedim, ensdim=ensdim)
        crps_m = np.nanmean(crps, axis=timedim)
        crps_s = 1-crps_m/crps_u

        # Compute Alpha
        pits = gridmetrics.pits(obs, ens, timedim=timedim, ensdim=ensdim)
        alpha = gridmetrics.alpha_cramer_vonmises(pits, \
                                                timedim=timedim).squeeze()

        # Compute log bias
        bias = np.log(1e-3+np.median(ens, axis=ensdim))-np.log(1e-3+obs)
        bias = np.nanmean(bias, axis=timedim)

        # Compute Q90 spread bias
        obs_Q90 = np.diff(np.nanpercentile(obs, [5, 95], \
                                            axis=timedim), axis=0)[0]
        ens_Q90 = np.diff(np.nanpercentile(ens, \
                                   [5, 95], axis=ensdim), axis=0)[0]
        ens_Q90 = np.mean(ens_Q90, axis=timedim)
        qi = np.log(1e-3+obs_Q90)-np.log(1e-3+ens_Q90)

    return crps_s, alpha, bias, qi


# Constrain certain weights to be zero
class ZeroWeights(tf.keras.constraints.Constraint):
  """Constrains certain weights to be zero. """

  def __init__(self, constraint):
    self.constraint = constraint

  def __call__(self, w):
    return tf.where(self.constraint==1, tf.zeros_like(w), w)

  def get_config(self):
    return {'constraint': self.constraint}


