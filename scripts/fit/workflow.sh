#!/bin/bash
   
# Folders
DIR_SCRIPT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Month
MONTH=$1

# XV configuration
XVCONFIG="years"

echo Fitting model for MONTH $MONTH

for training in "full"
do
    # Using linear regression 
    python $DIR_SCRIPT/fit.py -m $MONTH -xv $XVCONFIG -lr -tr $training
    python $DIR_SCRIPT/predict.py -m $MONTH -xv $XVCONFIG -lr -tr $training
    python $DIR_SCRIPT/verif.py -m $MONTH -xv $XVCONFIG -lr -tr $training
   
    # Using ml with obs rainfall
    for loss in "lossmse" "lossbayes"
    do
        python $DIR_SCRIPT/fit.py -m $MONTH -xv $XVCONFIG -lo $loss -tr $training
        python $DIR_SCRIPT/predict.py -m $MONTH -xv $XVCONFIG -lo $loss -tr $training
        python $DIR_SCRIPT/verif.py -m $MONTH -xv $XVCONFIG -lo $loss -tr $training
    done
done
