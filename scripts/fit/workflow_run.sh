#!/bin/bash
   
# Folders
DIR_SCRIPT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Number of cpus
NJOBS=4

# Run using GNU parallel for 12 months
nohup seq 1 12 | parallel -j $NJOBS "$DIR_SCRIPT/workflow.sh {}" & 1> nohup.out 2>nohup.err
