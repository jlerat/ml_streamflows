#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2021-02-11 Thu 01:12 PM
## Comment : plot verif metrics
##
## ------------------------------
import sys, os, re, json, math
import warnings
import glob
from calendar import month_abbr
import time, argparse
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib.gridspec import GridSpec
from matplotlib.backends.backend_pdf import PdfPages
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

source_file = os.path.abspath(__file__)
froot = os.path.join(os.path.dirname(source_file), '..', '..')

import importlib.util
spec = importlib.util.spec_from_file_location('data_utils', \
                os.path.join(froot, 'scripts', 'data', 'data_utils.py'))
data_utils = importlib.util.module_from_spec(spec)
spec.loader.exec_module(data_utils)

import fit_utils

warnings.filterwarnings('ignore', 'invalid value')

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------
parser = argparse.ArgumentParser(\
    description='Run ANN model', \
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-xv', '--xvconfig', \
                    help='Cross validation config', \
                    type=str, default='split')
args = parser.parse_args()
xvconfig = args.xvconfig

obs_color = 'red'
sim_color = 'steelblue'
bjp_color = 'green'

#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
fout = os.path.join(froot, 'outputs', 'fit')
fimg = os.path.join(froot, 'images')
os.makedirs(fimg, exist_ok=True)

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
LOGGER = data_utils.get_logger(basename)

#----------------------------------------------------------------------
# Get data
#----------------------------------------------------------------------
LOGGER.info('Loading data')

perf_files = glob.glob(os.path.join(fout, '**', 'verif_*.csv'))
scores = []
for f in perf_files:
    # Reads options
    basen = os.path.basename(f)
    xv = re.search('(?<=_XV)[^_]+(?=\.)', basen).group()
    if not xv == xvconfig:
        continue
    rf = re.search('(?<=_RF)[^_]+(?=_)', basen).group()
    lr = re.search('(?<=_LR)[^_]+(?=_)', basen).group()
    lo = re.search('(?<=_LO)[^_]+(?=_)', basen).group()
    if lr == 'true':
        model = 'Reg'
    else:
        model = f'ML\n{lo}'

    # Read data
    df = pd.read_csv(f)
    df.loc[:, 'model'] = model
    scores.append(df)

scores = pd.concat(scores)

fb = os.path.join(froot, 'data', 'bjp2_hindcasts', 'bjp2_hindcast_verif.csv')
bjp = pd.read_csv(fb)
bjp = bjp.loc[bjp.xvconfig == xvconfig, :]
bjp.loc[:, 'model'] = 'BJP'
scores = pd.concat([scores, bjp], sort=False, axis=0)

#---------------------------------------------------------------------
# Process
#----------------------------------------------------------------------
score_names = scores.score.unique()

for sname in score_names:
    plt.close('all')
    fig, axs = plt.subplots(ncols=4, nrows=3)
    axs = axs.flat

    for month in range(1, 13):
        idx = (scores.month == month) & (scores.score == sname)
        df = scores.loc[idx, :]

        models = df.model
        df = df.T.iloc[:-4, :]
        df.columns = models.values
        df = df.loc[pd.notnull(df).all(axis=1), :].astype(float)
        df = df.loc[:, np.sort(df.columns)]

        ax = axs[month-1]
        df.boxplot(ax=ax, showfliers=False, grid=False)

        title = month_abbr[month]
        ax.set_title(title)

        if sname in ['crps_ss', 'qi', 'bias']:
            ref = 0
        elif sname == 'alpha':
            ref = 0.05

        x0, x1 = ax.get_xlim()
        ax.plot([x0, x1], [ref]*2, 'k-', lw=0.9)
        ax.set_xlim((x0, x1))

        # Y axis limits
        if sname == 'crps_ss':
            ax.set_ylim((-0.5, 0.5))
        elif sname == 'bias':
            ax.set_ylim((-1, 1))
        elif sname == 'qi':
            ax.set_ylim((-1, 3))
        elif sname == 'alpha':
            ax.set_ylim((0, 1))
            forward = lambda x: np.sqrt(x)
            inverse = lambda x: x*x
            ax.set_yscale('function', functions=(forward, inverse))

    fig.set_size_inches((15, 10))
    fig.tight_layout(rect=[0, 0, 1, 0.95])
    fig.suptitle(f'Verification {sname}')
    fp = os.path.join(fimg, f'verif_{sname}.png')
    fig.savefig(fp)

LOGGER.info('Process completed')

