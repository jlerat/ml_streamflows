#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2021-01-30 Sat 03:00 PM
## Comment : Try simple ANN with streamflow
##
## ------------------------------
import sys, os, re, json, math
import time, argparse
import logging
from itertools import product as prod

import pandas as pd
import numpy as np
from scipy.stats import t as student
from scipy.spatial.distance import pdist, squareform

import tensorflow as tf
from tensorflow import keras
import tensorflow_probability as tfp

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

source_file = os.path.abspath(__file__)
froot = os.path.join(os.path.dirname(source_file), '..', '..')

import importlib.util
spec1 = importlib.util.spec_from_file_location('data_utils', \
                os.path.join(froot, 'scripts', 'data', 'data_utils.py'))
data_utils = importlib.util.module_from_spec(spec1)
spec1.loader.exec_module(data_utils)

spec2 = importlib.util.spec_from_file_location('data_utils', \
                os.path.join(froot, 'scripts', 'fit', 'fit_utils.py'))
fit_utils = importlib.util.module_from_spec(spec2)
spec2.loader.exec_module(fit_utils)
#import fit_utils

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------
parser = argparse.ArgumentParser(\
    description='Calibrate ANN model', \
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-m', '--month', \
                    help='Calendar month', \
                    type=int, default=1)
parser.add_argument('-xv', '--xvconfig', \
                    help='Cross validation config', \
                    type=str, default='years', \
                    choices=['split', 'full', 'years'])
parser.add_argument('-rf', '--rainfall_forecasts', \
                    help='Use rainfall forecasts as predictor', \
                    action='store_true', default=False)
parser.add_argument('-lr', '--linear_regression', \
                    help='Linear regression model', \
                    action='store_true', default=False)
parser.add_argument('-re', '--run_eagerly', \
                    help='Run keras model eagerly for debugging', \
                    action='store_true', default=False)
parser.add_argument('-lo', '--loss_func', \
                    help='Loss function', \
                    type=str, default='lossmse', \
                    choices=['lossmse', 'lossbayes'])
parser.add_argument('-tr', '--training_dataset', \
                    help='Volume of training data', \
                    type=str, default='full', \
                    choices=['full', 'monthly'])
args = parser.parse_args()
month = args.month
xvconfig = args.xvconfig
rainfall_forecasts = args.rainfall_forecasts
linear_regression = args.linear_regression
run_eagerly = args.run_eagerly
loss_func = args.loss_func
training_dataset = args.training_dataset

winit_stdev = 1e-2

# Values used to reduce number of parameters
# .. Max distrance radius of 100 km around catchment centroid
distance_radius = 100
# .. maximum drop in correlation in training obs outputs
#corr_delta = 0.3

# General config
nepochs, learning_rate, early_stopping_patience, \
    trans, zero_thresh, data_version, \
    sites_xlim, sites_ylim, year_start, year_end, \
    data_patterns, days_tol, leave_out = \
                        data_utils.get_config(rainfall_forecasts)

# define model name
model_name = fit_utils.get_model_name(month, rainfall_forecasts, \
                        linear_regression, loss_func, training_dataset)

#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
fout = os.path.join(froot, 'outputs', 'fit')
os.makedirs(fout, exist_ok=True)

flogs = os.path.join(froot, 'logs')
os.makedirs(flogs, exist_ok=True)

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
flog = os.path.join(flogs,  f'fit_{model_name}_XV{xvconfig}.log')
LOGGER = data_utils.get_logger(basename, flog)
data_utils.log_args(LOGGER, args)

#----------------------------------------------------------------------
# Get data
#----------------------------------------------------------------------
LOGGER.info('Loading training data')
sites = data_utils.load_sites(data_version)
sites, pattern_sites = data_utils.filter_sites(sites, sites_xlim, sites_ylim)
nsites = len(sites)

data = data_utils.load_data(data_version)
data = data_utils.filter_data(data, year_start, year_end, pattern_sites)

LOGGER.info(f'Define training and validation data for scheme {xvconfig}')
idx_dict = data_utils.get_xv_indices(xvconfig, month, data.index,\
                                    training_dataset, \
                                    days_tol=days_tol, \
                                    leave_out=leave_out)

#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------
for xvkey, idxs in idx_dict.items():
    LOGGER.info(f'XV {xvkey}')

    # Create model directories
    model_path = os.path.join(fout, model_name, \
                   f'XV{xvkey}', 'deterministic')
    os.makedirs(model_path, exist_ok=True)

    prob_model_path = os.path.join(model_path, '..', 'probabilistic')
    os.makedirs(prob_model_path, exist_ok=True)

    # Extract training and validation data
    extract = fit_utils.get_WXZ(data, idxs, data_patterns, trans)

    ###### Preprocessing layers ############################################
    preproc_layers = {}
    for vkey, vdata in extract.items():
        if vkey[0] == 'Z':
            continue

        # Basic input layer
        varname = vkey[1:]
        npreds = vdata['train']['nvar']
        in_layer = keras.layers.Input(shape=(npreds, ), \
                                            name=f'in_{varname}')

        # Power transform layer
        lam_trans = trans[vkey]['lam']
        eps_trans = trans[vkey]['eps']
        xmean = vdata['train']['trans_mean']
        trans_layer = keras.layers.Lambda(fit_utils.datatrans_tf, \
                               output_shape=(npreds, ), \
                               arguments={'xmean': xmean, \
                                            'lam_trans':lam_trans,\
                                            'eps_trans': eps_trans, \
                                            'zero_thresh': zero_thresh}, \
                               trainable=False, \
                               name=f'trans_{varname}')(in_layer)

        # Normalisation layer
        xstd = vdata['train']['trans_std']
        norm_layer = keras.layers.Dense(npreds, \
                        weights=[np.diag(1/xstd), -xmean/xstd], \
                        name=f'normalise_{varname}', \
                        trainable=False)(trans_layer)

        preproc_layers[vkey] = {
            'input': in_layer, \
            'trans': trans_layer, \
            'norm': norm_layer
        }

    ######## Linear regression in transform space layer #################
    xnorm = extract['Wrunoff']['train']['trans_norm']
    xnorm[np.isnan(xnorm)] = 0
    ynorm = extract['Z']['train']['trans_norm']
    ynorm[np.isnan(ynorm)] = 0
    theta, sse, _, _ = np.linalg.lstsq(xnorm, ynorm, rcond=None)

    # Detect variable with low correlation
    coords = sites.loc[:, ['lon_centroid_dem', 'lat_centroid_dem']].values
    dist = squareform(pdist(coords))*4e4/360
    izero = [np.where(d>distance_radius)[0] for d in dist]

    # Adjust to remove insignificant parameters
    # See
    # https://en.wikipedia.org/wiki/Ordinary_least_squares#Constrained_estimation
    tXXinv = np.linalg.inv(xnorm.T.dot(xnorm))
    theta_adjust = np.zeros_like(theta)
    constraint = np.zeros_like(theta).astype(np.int32)
    for i in range(nsites):
        # Compute adjustment matrix
        iz = izero[i]
        nc = len(iz)
        Q = np.zeros((nc, nsites))
        Q[np.arange(nc), iz] = 1
        constraint[iz, i] = 1
        M = np.linalg.inv(Q.dot(tXXinv).dot(Q.T))

        # Adjust
        t = theta[:, i]
        ta = t-tXXinv.dot(Q.T.dot(M.dot(Q.dot(t))))
        #ta[np.abs(ta) < 1e-7] = 0.
        theta_adjust[:, i] = ta

    norm_runoff_layer = preproc_layers['Wrunoff']['norm']
    npreds = extract['Wrunoff']['train']['nvar']
    reg_runoff_layer = keras.layers.Dense(npreds, \
              weights=[theta_adjust, np.zeros(npreds)], \
              name='regression_runoff', trainable=False)(norm_runoff_layer)

    ######## ANN layers ###############################################
    layers = [preproc_layers['Wclim']['norm'],
                preproc_layers['Wrain']['norm'],
                reg_runoff_layer]
    concat_in = keras.layers.Concatenate(axis=-1, name='concat_in', \
                                    trainable=False)(layers)

    # No constraint on clim indices but constraint on rainfall and streamflow
    nclim = extract['Wclim']['train']['nvar']
    const1 = np.row_stack([np.zeros((nclim, nsites))]+[constraint]*2)

    hidden1_layer = keras.layers.Dense(nsites, activation='elu', \
                       kernel_constraint=fit_utils.ZeroWeights(const1), \
                       kernel_initializer=\
                         keras.initializers.RandomNormal(stddev=winit_stdev),
                       bias_initializer=\
                               keras.initializers.Zeros(), \
                       name='hidden1')(concat_in)

    hidden2_layer = keras.layers.Dense(nsites, \
                       kernel_constraint=fit_utils.ZeroWeights(constraint), \
                       kernel_initializer=\
                         keras.initializers.RandomNormal(stddev=winit_stdev),
                       bias_initializer=keras.initializers.Zeros(), \
                       name='hidden2')(hidden1_layer)

    concat_out = keras.layers.Concatenate(axis=-1, name='concat_out', \
                                    trainable=False)(\
                                        [hidden2_layer, reg_runoff_layer])

    if linear_regression:
        w = np.row_stack([0*np.eye(nsites), np.eye(nsites)])
    else:
        w = np.row_stack([np.eye(nsites), np.eye(nsites)])
    output_layer = keras.layers.Dense(nsites, name='output', \
                            weights=[w, np.zeros(nsites)], \
                            trainable=False)(concat_out)

    ######## Post processing layers ##################################
    ymean = extract['Z']['train']['trans_mean']
    ystd = extract['Z']['train']['trans_std']
    denorm_layer = keras.layers.Dense(nsites, \
              weights=[np.diag(ystd), ymean], \
              name='denormalise', trainable=False)(output_layer)

    lam_trans = trans['Z']['lam']
    eps_trans = trans['Z']['eps']
    invdatatrans_layer = keras.layers.Lambda(fit_utils.invdatatrans_tf, \
                            output_shape=(nsites, ), \
                            arguments={'lam_trans': lam_trans, \
                                        'eps_trans': eps_trans}, \
                            name='invdatatrans')(denorm_layer)

    positive_layer = keras.layers.Lambda(fit_utils.positive_tf, \
                            output_shape=(nsites, ), \
                            name='positive')(invdatatrans_layer)

    ####################  Build model ###############################
    LOGGER.info('.. Building model')
    name = f'{model_name}_xv{xvconfig}'
    input_vkeys = ['Wclim', 'Wrain', 'Wrunoff']
    input_layers = [preproc_layers[vkey]['input'] for vkey in input_vkeys]

    model = keras.Model(name=name, \
                        inputs=input_layers, \
                        outputs=[denorm_layer, positive_layer])

    optimizer = keras.optimizers.Adam(learning_rate=learning_rate, \
                                        beta_1=0.9, beta_2=0.999, \
                                        epsilon=1e-3)

    loss = getattr(fit_utils, loss_func)
    model.compile(loss=[loss, fit_utils.loss_dummy], \
                optimizer=optimizer, \
                metrics=loss, loss_weights=[1., 0.], \
                run_eagerly=run_eagerly)

    ####################  Model fitting ###############################
    LOGGER.info('.. Fitting model')

    # CSV logger callback
    flogk = re.sub('\.log', '_keraslog.csv', flog)
    csv_logger = keras.callbacks.CSVLogger(flogk, append=True, separator=',')

    # Early stopping call back to stop training when
    # validation loss decreases
    early_stopping_cb = tf.keras.callbacks.EarlyStopping(\
                                    monitor='val_loss', \
                                    patience=early_stopping_patience, \
                                    restore_best_weights=True)

    xtrain = [extract[vkey]['train']['raw'] for vkey in input_vkeys]
    xvalid = [extract[vkey]['valid']['raw'] for vkey in input_vkeys]

    ytrain, ztrain = [extract['Z']['train'][dkey] \
                            for dkey in ['trans', 'raw']]
    yvalid, zvalid = [extract['Z']['valid'][dkey] \
                            for dkey in ['trans', 'raw']]
    nval = ytrain.shape[0]

    history = model.fit(x=xtrain, y=[ytrain, ztrain], \
                epochs=nepochs, \
                validation_data=(xvalid, [yvalid, zvalid]), \
                callbacks=[early_stopping_cb, csv_logger], \
                batch_size=nval)

    ####################  Build probabilistic model #########################

    # Compute output covariance from validation data
    ypred, zpred = model.predict(xvalid)
    err = ypred-yvalid
    err_mean = np.nanmean(err, axis=0)
    inan = np.where(np.isnan(err))
    err[inan[0], inan[1]] = err_mean[inan[1]]
    err_cov = np.cov(err.T).astype(np.float32)
    # .. check semi-definite pos
    tf.linalg.cholesky(err_cov)

    # Create randomisation layer
    prob_denorm_layer = tfp.layers.DistributionLambda(lambda t: \
                          tfp.distributions.MultivariateNormalFullCovariance(
                                    loc=t-err_mean, covariance_matrix=err_cov), \
                                        name='prob_denormalise')(denorm_layer)
    lam_trans = trans['Z']['lam']
    eps_trans = trans['Z']['eps']
    prob_invdatatrans_layer = keras.layers.Lambda(fit_utils.invdatatrans_tf, \
                            output_shape=(nsites, ), \
                            arguments={'lam_trans': lam_trans, \
                                        'eps_trans': eps_trans}, \
                            name='prob_invdatatrans')(prob_denorm_layer)

    prob_positive_layer = keras.layers.Lambda(fit_utils.positive_tf, \
                            output_shape=(nsites, ), \
                            name='prob_positive')(prob_invdatatrans_layer)

    prob_model = keras.Model(name=name+'_probabilistic', \
                        inputs=input_layers, \
                        outputs=prob_positive_layer)

    # Diagnostic plots
    LOGGER.info('.. Generate diagnostic plots')
    plt.close('all')
    fig, axs = plt.subplots(ncols=2)

    for ax, val in zip(axs.flat, ['', 'val_']):
        name = f'{val}denormalise_loss'
        ax.plot(history.history[name], label=name)

        ax.legend(loc=1)
        title = 'Fit' if val == '' else 'Valid'
        ax.set_title(title)
        ax.set_xlabel('Epoch')
        ax.set_ylabel('Loss')

    fig.set_size_inches((10, 7))
    fp = os.path.join(model_path, '..', 'fit_history_deterministic.png')
    fig.savefig(fp)

    #fpdf = os.path.join(model_path, '..', 'fit_scatter_transform.pdf')
    #with PdfPages(fpdf) as pdf:
    #    for isite, cn in enumerate(Zcols):
    #        siteid = re.sub('_.*', '', re.sub('.*ff_', '', cn))
    #        iplot = isite%9
    #        if iplot == 0:
    #            plt.close('all')
    #            fig, axs = plt.subplots(ncols=3, nrows=3)
    #            axs = axs.flat
    #
    #        ax = axs[iplot]
    #        ax.plot(Ypred[:, isite], Yvalid[:, isite], 'o', \
    #                    markersize=5, alpha=0.3)
    #
    #        (x0, x1), (y0, y1) = ax.get_xlim(), ax.get_ylim()
    #        ax.plot([x0, x1], [x0, x1], 'k-', lw=0.9)
    #        ax.set_xlim([x0, x1])
    #        ax.set_ylim([y0, y1])
    #        title = f'Site {isite} - {siteid}'
    #        ax.set_title(title)
    #        ax.set_xlabel('Ypred')
    #        ax.set_ylabel('Ytrain')
    #
    #        if iplot == 8:
    #            fig.set_size_inches((12, 12))
    #            fig.tight_layout(rect=[0, 0, 1, 0.95])
    #            fig.suptitle(f'Month {month} xv {xvconfig}')
    #            pdf.savefig(fig)

    LOGGER.info('.. Define model layers')


    ####################  Model saving ###############################
    LOGGER.info('.. Saving models')

    # Save model
    fm = os.path.join(model_path, f'{model.name}.h5')
    #model.save(fm)
    tf.saved_model.save(model, model_path)

    fm = os.path.join(prob_model_path, f'{prob_model.name}.h5')
    #prob_model.save(fm)
    tf.saved_model.save(prob_model, prob_model_path)


LOGGER.info('Process completed')

