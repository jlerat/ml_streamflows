#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2021-01-30 Sat 03:00 PM
## Comment : Try simple ANN with streamflow
##
## ------------------------------
import sys, os, re, json, math
import time, argparse
from itertools import product as prod
import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow import keras
from scipy.stats import spearmanr

import matplotlib.pyplot as plt

from hydrodiy.io import csv, iutils
from hydrodiy.plot import putils

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------
parser = argparse.ArgumentParser(\
    description='Calibrate ANN model', \
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-v', '--version', \
                    help='Version number', \
                    type=str, required=True)
parser.add_argument('-x', '--xvconfig', \
                    help='XV year/scheme', \
                    type=str, default='split')
parser.add_argument('-m', '--month', \
                    help='Calendar month', \
                    type=int, default=1)
parser.add_argument('-e', '--nepochs', \
                    help='Numpber of epochs', \
                    type=int, default=100)
parser.add_argument('-l', '--learning_rate', \
                    help='Learning rate', \
                    type=float, default=5e-3)
parser.add_argument('-b', '--lam_boxcox', \
                    help='Box cox transform exponent.', \
                    type=float, default=0.3)
parser.add_argument('-s', '--early_stopping_patience', \
                    help='Define early stopping patience. <=0 removes it.',\
                    type=int, default=10)
args = parser.parse_args()

version = args.version
nepochs = args.nepochs
learning_rate = args.learning_rate
xvconfig = args.xvconfig
month = args.month
early_stopping_patience = args.early_stopping_patience
lam_boxcox = args.lam_boxcox

# Using future rainfall!
pattern_por_rain = '^por_rain.*n10d$'

# Using recent runoff
pattern_por_runoff = '^por_runoff.*p20d$'

# One month ahead streamflow forecast
pattern_pand = '^pand_runoff.*n30d$'

nleaveout = 5

#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
source_file = os.path.abspath(__file__)
froot = os.path.join(os.path.dirname(source_file), '..', '..')

fdata = os.path.join(froot, 'data')

fout = os.path.join(froot, 'outputs', 'fit_test')
os.makedirs(fout, exist_ok=True)

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
LOGGER = iutils.get_logger(basename)

#----------------------------------------------------------------------
# Get data
#----------------------------------------------------------------------
fs = os.path.join(fdata, f'streamflow_sites_version{version}.csv')
sites, _ = csv.read_csv(fs, index_col=0, parse_dates=True)

idx = (sites.lon_centroid_dem > 148.5) & (sites.lat_centroid_dem < -28.6)
pattern_sites = '|'.join([str(siteid) \
                            for siteid in sites.index[idx].tolist()]) \
                + '.*'
fss = os.path.join(fout, f'streamflow_sites_test.csv')
sites = sites.loc[idx, :]
csv.write_csv(sites, fss, 'Selected streamflow sites', \
                source_file, compress=False, write_index=True)

fd = os.path.join(fdata, f'streamflow_data_version{version}.csv')
data, _ = csv.read_csv(fd, index_col=0, parse_dates=True)
data = data.loc['1981':, :].filter(regex=pattern_sites, axis=1)

#----------------------------------------------------------------------
# Utils functions
#----------------------------------------------------------------------
def get_run_logdir():
    run_id = time.strftime('run_%Y_%m_%d-%H_%M_%S')
    return os.path.join(flogs, run_id)

#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------

# Define train and test set
if not xvconfig in ['full', 'split']:
    xvper = np.arange(xvconfig, xvconfig+nleaveout)
    idx_train = ~data.index.year.isin(xvper) \
                            & (data.index.month == month)
    idx_valid = (data.index.year == xvconfig) \
                            & (data.index.month == month)
elif xvconfig == 'full':
    # Train on full set
    idx_train = data.index.month == month
    idx_valid = idx_train
elif xvconfig == 'split':
    # Train on full set
    years = np.unique(data.index.year)
    idx_train = data.index.year.isin(years[::2])
    idx_valid = data.index.year.isin(years[1::2])

# Transform data
def datatrans_numpy(x, lam_boxcox):
    return np.power(x+1e-3, lam_boxcox)

Wtrain_rain = data.loc[idx_train, :].filter(regex=pattern_por_rain, axis=1)
Wtrain_runoff = data.loc[idx_train, :].filter(regex=pattern_por_runoff, \
                                                                    axis=1)
Wcols_rain = Wtrain_rain.columns.tolist()
Wcols_runoff = Wtrain_runoff.columns.tolist()

Wtrain_rain = Wtrain_rain.values.astype(np.float32)
Xtrain_rain = datatrans_numpy(Wtrain_rain, lam_boxcox)
Wtrain_runoff = Wtrain_runoff.values.astype(np.float32)
Xtrain_runoff = datatrans_numpy(Wtrain_runoff, lam_boxcox)

Ztrain = data.loc[idx_train, :].filter(regex=pattern_pand, axis=1)
Zcols = Ztrain.columns.tolist()
Ztrain = Ztrain.values.astype(np.float32)
Ytrain = datatrans_numpy(Ztrain, lam_boxcox)

Wvalid_rain = data.loc[idx_valid, :].filter(regex=pattern_por_rain, axis=1)
Wvalid_rain = Wvalid_rain.values.astype(np.float32)
Wvalid_runoff = data.loc[idx_valid, :].filter(regex=pattern_por_runoff, \
                                                    axis=1)
Wvalid_runoff = Wvalid_runoff.values.astype(np.float32)

Zvalid = data.loc[idx_valid, :].filter(regex=pattern_pand, axis=1)
Zvalid = Zvalid.values.astype(np.float32)
Yvalid = datatrans_numpy(Zvalid, lam_boxcox)

nsites = Ytrain.shape[1]
npreds_rain = Wtrain_rain.shape[1]
npreds_runoff = Wtrain_runoff.shape[1]

# Save training / valid data
model_path = os.path.join(fout, \
                f'ml_streamflow_month{month:02d}_xv{xvconfig}')
os.makedirs(model_path, exist_ok=True)
fd = os.path.join(model_path, \
                f'training_config_month{month:02d}_xv{xvconfig}.npz')
dd = {'Wcols_rain': Wcols_rain, \
        'Wcols_runoff': Wcols_runoff, \
        'Zcols': Zcols, \
        'idx_valid': idx_valid, 'idx_train': idx_train}
np.savez_compressed(fd, **dd)

# Initialise model
input_rain_layer = keras.layers.Input(shape=(npreds_rain, ), \
                                        name='input_rain')
input_runoff_layer = keras.layers.Input(shape=(npreds_runoff, ), \
                                        name='input_runoff')

# Normalising constants
Xmean_rain = tf.constant(np.nanmean(Xtrain_rain, axis=0))
Xmean_runoff = tf.constant(np.nanmean(Xtrain_runoff, axis=0))
Ymean = tf.constant(np.nanmean(Ytrain, axis=0))
Xstd_rain = tf.constant(np.nanstd(Xtrain_rain, axis=0))
Xstd_runoff = tf.constant(np.nanstd(Xtrain_runoff, axis=0))
Ystd = tf.constant(np.nanstd(Ytrain, axis=0))

###### Pre-processing layer - log transform #########################
def datatrans(w, xmean, lam_boxcox):
    eps = 1e-3
    zerothresh = 1e-4
    iszero = (w>=0) & (w<zerothresh)
    low = tf.math.pow(eps+zerothresh, lam_boxcox)*tf.ones_like(w)
    x = tf.where(iszero, low, tf.math.pow(w+eps, lam_boxcox))
    x = tf.where(w<0, xmean, x)
    return x

datatrans_rain_layer = keras.layers.Lambda(datatrans, \
                           output_shape=(npreds_rain, ), \
                           arguments={'xmean': Xmean_rain.numpy(), \
                                            'lam_boxcox':lam_boxcox}, \
                           trainable=False, \
                           name='datatrans_rain')(input_rain_layer)
datatrans_runoff_layer = keras.layers.Lambda(datatrans, \
                           output_shape=(npreds_runoff, ), \
                           arguments={'xmean': Xmean_runoff.numpy(), \
                                            'lam_boxcox':lam_boxcox},\
                           trainable=False, \
                           name='datatrans_runoff')(input_runoff_layer)

# ... check log transform works
model_test1 = keras.Model(inputs=input_rain_layer, \
                                outputs=datatrans_rain_layer)
test1 = model_test1.predict(Wtrain_rain)
err1 = np.abs(test1-datatrans_numpy(Wtrain_rain, lam_boxcox))
errmax = (err1[Wtrain_rain>1e-4]).max()
assert errmax < 5e-6
assert np.sum(np.isnan(test1)) == 0

####### Pre-processing layer - normalisation #######################
norm_rain_layer = keras.layers.Dense(npreds_rain, \
          weights=[np.diag(1/Xstd_rain.numpy()), \
                           -Xmean_rain.numpy()/Xstd_rain.numpy()], \
          name='normalise_rain', trainable=False)(datatrans_rain_layer)
norm_runoff_layer = keras.layers.Dense(npreds_runoff, \
          weights=[np.diag(1/Xstd_runoff.numpy()), \
                           -Xmean_runoff.numpy()/Xstd_runoff.numpy()], \
          name='normalise_runoff', trainable=False)(datatrans_runoff_layer)

# ... check normalisation works
model_test2 = keras.Model(inputs=input_rain_layer, outputs=norm_rain_layer)
test2 = model_test2.predict(Wtrain_rain)
err2 = np.abs(test2-(Xtrain_rain-Xmean_rain[None, :])/Xstd_rain[None, :])
errmax = (err2[Wtrain_rain>1e-4]).max()
assert errmax < 5e-4
assert np.sum(np.isnan(test2)) == 0


######## Linear regression #########################################
xm = Xmean_runoff.numpy()[None, :]
x = Xtrain_runoff-xm
x[np.isnan(x)] = 0

ym = Ymean.numpy()[None, :]
y = Ytrain-ym
y[np.isnan(y)] = 0

theta, _, _, _ = np.linalg.lstsq(x, y, rcond=None)
reg_runoff_layer = keras.layers.Dense(npreds_runoff, \
          weights=[theta, np.zeros(npreds_runoff)], \
          name='regression_runoff', trainable=False)(norm_runoff_layer)

# ... check regression model
#model_test3 = keras.Model(inputs=input_runoff_layer, \
#                                outputs=reg_runoff_layer)
#test3 = model_test3.predict(Wtrain_runoff)

# Custom constraint
#class KeepDiagonalOnly(tf.keras.constraints.Constraint):
#
#    def __init__(self):
#        pass
#
#    def __call__(self, W):
#        Wdiag = tf.linalg.set_diag(tf.zeros_like(W), \
#                    tf.linalg.diag_part(W), name=None)
#        return Wdiag

# ANN layers
concat_in_layer = keras.layers.Concatenate(axis=-1, name='concat_in', \
                    trainable=False)([norm_rain_layer, norm_runoff_layer])

nhidden = nsites//20
hidden1_layer = keras.layers.Dense(nhidden, activation='elu', \
                        kernel_initializer=\
                                keras.initializers.RandomNormal(stddev=1e-3),
                        bias_initializer=\
                                keras.initializers.Zeros(), \
                        #kernel_constraint=KeepDiagonalOnly(), \
                        name='hidden1')(concat_in_layer)

hidden2_layer = keras.layers.Dense(nsites, \
                        kernel_initializer=\
                                keras.initializers.RandomNormal(stddev=1e-3),
                        bias_initializer=keras.initializers.Zeros(), \
                        name='hidden2')(hidden1_layer)

concat_out_layer = keras.layers.Concatenate(axis=-1, name='concat_out', \
                        trainable=False)([reg_runoff_layer, hidden2_layer])

w = np.row_stack([np.eye(nsites), np.eye(nsites)])
output_layer = keras.layers.Dense(nsites, name='output', \
                        weights=[w, np.zeros(nsites)], \
                        trainable=False)(concat_out_layer)

# Post-processing layers
denorm_layer = keras.layers.Dense(nsites, \
          weights=[np.diag(Ystd.numpy()), Ymean.numpy()], \
          name='denormalise', trainable=False)(output_layer)

denorm_reg_runoff_layer = keras.layers.Dense(nsites, \
          weights=[np.diag(Ystd.numpy()), Ymean.numpy()], \
          name='denormalise_reg', trainable=False)(reg_runoff_layer)

def invdatatrans_tf(y, lam_boxcox):
    return tf.math.pow(tf.maximum(y, 1e-6), 1./lam_boxcox)-1e-3
invdatatrans_layer = keras.layers.Lambda(invdatatrans_tf, \
                        output_shape=(nsites, ), \
                        arguments={'lam_boxcox': lam_boxcox}, \
                        name='invdatatrans')(denorm_layer)
def positive_tf(y):
    return tf.where(y<0, tf.zeros_like(y), y)
positive_layer = keras.layers.Lambda(positive_tf, \
                        output_shape=(nsites, ), \
                        name='positive')(invdatatrans_layer)

# Build model
name = f'ml_streamflow_month{month:02d}_xv{xvconfig}'
model = keras.Model(name=name, \
                    inputs=[input_rain_layer, input_runoff_layer], \
                    outputs=[denorm_layer, positive_layer, \
                                denorm_reg_runoff_layer])

# .. alternative build to monitor gradient
# see https://keras.io/examples/keras_recipes/debugging_tips/
#           #tip-3-to-debug-what-happens-during-fit-use-runeagerlytrue
#class DebugModel(keras.Model):
#    def train_step(self, data):
#        print()
#        print(f"----Start of step: ")
#        inputs, targets = data
#        trainable_vars = self.trainable_variables
#        with tf.GradientTape() as tape:
#            preds = self(inputs, training=True)  # Forward pass
#            # Compute the loss value
#            # (the loss function is configured in `compile()`)
#            loss = self.compiled_loss(targets, preds)
#        # Compute first-order gradients
#        grads = tape.gradient(loss, trainable_vars)
#        print(f"Max of dl_dw[0]: {tf.reduce_max(grads[0]):0.4f}")
#        print(f"Min of dl_dw[0]: {tf.reduce_min(grads[0]):0.4f}")
#        print(f"Mean of dl_dw[0]:{tf.reduce_mean(grads[0]):0.4f}")

#        # Update weights
#        self.optimizer.apply_gradients(zip(grads, trainable_vars))

#        # Update metrics (includes the metric that tracks the loss)
#        self.compiled_metrics.update_state(targets, preds)
#        # Return a dict mapping metric names to current value
#        return {m.name: m.result() for m in self.metrics}

#model = DebugModel(name=name, inputs=input_layer, \
#                        outputs=[output_layer, positive_layer])

# Define loss functions
def loss_nonan(obs, pred):
    err = tf.maximum(tf.abs(obs-pred), 1e-5)
    err = tf.square(err)
    isnan = tf.math.is_nan(err)
    return tf.where(isnan, tf.zeros_like(obs), err)

def loss_mean(obs, pred):
    err_nonan = loss_nonan(obs, pred)
    return tf.sqrt(tf.math.reduce_mean(err_nonan, axis=-1))

def dummy(obs, pred):
    return tf.zeros_like(obs)

optimizer = keras.optimizers.Adam(learning_rate=learning_rate, \
                                    beta_1=0.9, beta_2=0.999, \
                                    epsilon=1e-3)
#optimizer = keras.optimizers.SGD(learning_rate=learning_rate, \
#                                    momentum=1.)

model.compile(loss=[loss_mean, dummy, loss_mean], \
            optimizer=optimizer, \
            metrics=loss_mean, loss_weights=[1., 0., 0.], \
            run_eagerly=True)


if early_stopping_patience > 1:
    # Early stopping call back to stop training when
    # validation loss decreases
    early_stopping_cb = tf.keras.callbacks.EarlyStopping(\
                                    monitor='val_loss', \
                                    patience=early_stopping_patience, \
                                    restore_best_weights=True)
    history = model.fit(\
                x=[Wtrain_rain, Wtrain_runoff], \
                y=[Ytrain, Ztrain, Ytrain], \
                epochs=nepochs, \
                validation_data=([Wvalid_rain, Wvalid_runoff], \
                                        [Yvalid, Zvalid, Yvalid]), \
                callbacks=[early_stopping_cb])

                # To train over the whole lot
                #batch_size=len(Ytrain))
else:
    history = model.fit(\
                x=[Wtrain_rain, Wtrain_runoff], \
                y=[Ytrain, Ztrain, Ytrain], \
                epochs=nepochs, \
                validation_data=([Wvalid_rain, Wvalid_runoff], \
                                        [Yvalid, Zvalid, Yvalid]))

                # To train over the whole lot
                #batch_size=len(Ytrain))

# Evaluate
Ypred, Zpred, Ypredreg = model.predict([Wvalid_rain, Wvalid_runoff])

loss = loss_nonan(Yvalid, Ypred).numpy()
lossmean = loss_mean(Yvalid, Ypred).numpy().mean()
lossreg = loss_nonan(Yvalid, Ypredreg).numpy()
lossmeanreg = loss_mean(Yvalid, Ypredreg).numpy().mean()

# Simple prediction plot
plt.close('all')
z = Ztrain.copy()
z[z<0] = np.nan

def metrics(obs, sim):
    idx = ~np.isnan(obs)
    rsp = spearmanr(obs[idx], sim[idx])[0]
    v = np.sum((np.mean(obs[idx])-obs[idx])**2)
    nse = 1-np.sum((sim[idx]-obs[idx])**2)/v
    return rsp, nse

plt.close('all')
fig, axs = plt.subplots(ncols=4, nrows=3)
for i, ax in enumerate(axs.flat):
    if i>=nsites:
        break
    u, v = Ypred[:, i], Yvalid[:, i]
    ax.plot(u, v, 'o', alpha=0.2, markersize=3, label='pred')
    rsp, nse = metrics(v, u)

    u, v = Ypredreg[:, i], Yvalid[:, i]
    ax.plot(u, v, 'o', alpha=0.2, markersize=3, label='reg')
    rspr, nser = metrics(v, u)

    lab = re.sub('.*ff_|_n.*', '', Zcols[i])
    title = f'{lab}\nPred: R2 = {rsp:0.2f} NSE={nse:0.2f}\n'+\
            f'Reg: R2 = {rspr:0.2f} NSE={nser:0.2f}'
    ax.set_title(title)
    putils.line(ax, 1, 1, 0, 0, 'k-', lw=0.5)

    ax.set_xlabel('pred')
    ax.set_ylabel('obs')
    ax.legend(loc=2)

fig.suptitle(f'Validation Loss = {lossmean:0.3f}'+\
                f' Loss reg={lossmeanreg:0.3f}')
fig.set_size_inches((12, 9))
fig.tight_layout(rect=[0, 0 , 1, 0.95])
fp = os.path.join(model_path, f'pred_diagnostic.png')
fig.savefig(fp)

# Plot fitting results
plt.close('all')
fig, axs = plt.subplots(ncols=2)

for ax, val in zip(axs.flat, ['', 'val_']):
    name = f'{val}denormalise_loss'
    ax.plot(history.history[name], label=name)

    name = f'{val}denormalise_reg_loss'
    ax.plot(history.history[name], label=name)

    ax.legend(loc=1)
    title = 'Fit' if val == '' else 'Valid'
    ax.set_title(title)

fig.set_size_inches((10, 7))
fp = os.path.join(model_path, 'history.png')
fig.savefig(fp)

# Save model
fm = os.path.join(model_path, f'{os.path.split(model_path)[-1]}.h5')
model.save(fm)
tf.saved_model.save(model, model_path)


LOGGER.info('Process completed')

