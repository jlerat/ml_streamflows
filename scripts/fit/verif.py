#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2021-01-30 Sat 03:00 PM
## Comment : Run ANN predict
##
## ------------------------------
import sys, os, re, json, math
import logging
import time, argparse
import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow import keras

from gridverif import gridmetrics

source_file = os.path.abspath(__file__)
froot = os.path.join(os.path.dirname(source_file), '..', '..')

import importlib.util
spec = importlib.util.spec_from_file_location('data_utils', \
                os.path.join(froot, 'scripts', 'data', 'data_utils.py'))
data_utils = importlib.util.module_from_spec(spec)
spec.loader.exec_module(data_utils)

import fit_utils

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------
parser = argparse.ArgumentParser(\
    description='Compute verif stats for ML model', \
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-m', '--month', \
                    help='Calendar month', \
                    type=int, default=1)
parser.add_argument('-xv', '--xvconfig', \
                    help='Cross validation config', \
                    type=str, default='years')
parser.add_argument('-rf', '--rainfall_forecasts', \
                    help='Use rainfall forecasts as predictor', \
                    action='store_true', default=False)
parser.add_argument('-lr', '--linear_regression', \
                    help='Linear regression model', \
                    action='store_true', default=False)
parser.add_argument('-lo', '--loss_func', \
                    help='Loss function', \
                    type=str, default='lossmse', \
                    choices=['lossmse', 'lossbayes'])
parser.add_argument('-tr', '--training_dataset', \
                    help='Volume of training data', \
                    type=str, default='full', \
                    choices=['full', 'monthly'])
parser.add_argument('-ne', '--nens', \
                  help='Number of ensembles.', \
                  type=int, default=500)
args = parser.parse_args()
month = args.month
xvconfig = args.xvconfig
rainfall_forecasts = args.rainfall_forecasts
linear_regression = args.linear_regression
loss_func = args.loss_func
training_dataset = args.training_dataset
nens = args.nens

# General config
nepochs, learning_rate, early_stopping_patience, \
    trans, zero_thresh, data_version, \
    sites_xlim, sites_ylim, year_start, year_end, \
    data_patterns, days_tol, leave_out = \
                        data_utils.get_config(rainfall_forecasts)

# define model name
model_name = fit_utils.get_model_name(month, rainfall_forecasts, \
                        linear_regression, loss_func, training_dataset)

#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
fout = os.path.join(froot, 'outputs', 'fit')

flogs = os.path.join(froot, 'logs')
os.makedirs(flogs, exist_ok=True)

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
flog = os.path.join(flogs,  f'verif_{model_name}_XV{xvconfig}.log')
LOGGER = data_utils.get_logger(basename, flog)
data_utils.log_args(LOGGER, args)

#----------------------------------------------------------------------
# Get data
#----------------------------------------------------------------------
LOGGER.info('Loading data')
sites = data_utils.load_sites(data_version)
sites, pattern_sites = data_utils.filter_sites(sites, sites_xlim, sites_ylim)
nsites = len(sites)

data = data_utils.load_data(data_version)
data = data_utils.filter_data(data, year_start, year_end, pattern_sites)

LOGGER.info(f'Define training and validation data for scheme {xvconfig}')

# Use days_tol = 0 to keep values within a single month
idx_dict = data_utils.get_xv_indices(xvconfig, month, data.index,\
                                    training_dataset, \
                                    days_tol=0, \
                                    leave_out=leave_out)

#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------
Zobs = []
Zpred_ens = []
dates_pred = []

for xvkey, idxs in idx_dict.items():
    LOGGER.info(f'XV {xvkey}')

    # Create model directories
    model_path = os.path.join(fout, model_name, \
                   f'XV{xvkey}', 'deterministic')
    os.makedirs(model_path, exist_ok=True)

    prob_model_path = os.path.join(model_path, '..', 'probabilistic')
    os.makedirs(prob_model_path, exist_ok=True)

    # Extract training and validation data
    extract = fit_utils.get_WXZ(data, idxs, data_patterns, trans)
    input_vkeys = ['Wclim', 'Wrain', 'Wrunoff']
    xtest = [extract[vkey]['test']['raw'] for vkey in input_vkeys]
    ztest = extract['Z']['test']['raw']
    zcols = extract['Z']['test']['cols']

    # prediction dates
    idx_test = idxs['test']
    idx_pred = data.index[idx_test].day == 1

    d_pred = data.index[idx_test][idx_pred]

    LOGGER.info(f'XV{xvkey} - Loading probabilistic model')
    saved_model_prob = tf.saved_model.load(prob_model_path)

    LOGGER.info(f'XV{xvkey} - Run ensemble simulations')
    Zens = []
    for iens in range(nens):
        Z = saved_model_prob(xtest, training=False)
        Zens.append(Z.numpy()[idx_pred, :])

    Zens = np.array(Zens).T

    # Store
    Zpred_ens.append(Zens)
    Zobs.append(ztest[idx_pred, :].T)
    dates_pred.append(d_pred)

Zobs = np.array(Zobs).squeeze()
Zpred_ens = np.array(Zpred_ens).squeeze()
dates_pred = np.array(dates_pred).squeeze()

if xvconfig == 'years':
    Zpred_ens = np.moveaxis(Zpred_ens, 1, 0)
    Zobs = Zobs.T

LOGGER.info(f'Compute metrics')
crps_s, alpha, bias, qi = fit_utils.verif_scores(Zobs, Zpred_ens, \
                                    timedim=1, ensdim=2)

scores = pd.DataFrame(np.nan, index=np.arange(4), columns=sites.index)
scores.loc[0, :] = crps_s
scores.loc[1, :] = alpha
scores.loc[2, :] = bias
scores.loc[3, :] = qi
scores.loc[:, 'score'] = ['crps_ss', 'alpha', 'bias', 'qi']
scores.loc[:, 'month'] = month
scores.loc[:, 'xvconfig'] = xvconfig

fs = os.path.join(prob_model_path, '..', '..', \
            f'verif_{model_name}_XV{xvconfig}.csv')
scores.to_csv(fs, index=False, float_format='%0.3f')

LOGGER.info('Process completed')

