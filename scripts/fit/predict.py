#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2021-01-30 Sat 03:00 PM
## Comment : Run ANN predict
##
## ------------------------------
import sys, os, re, json, math
import warnings
import logging
import time, argparse

import pandas as pd
import numpy as np
from scipy.special import kolmogi

import tensorflow as tf
from tensorflow import keras

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib.gridspec import GridSpec
from matplotlib.backends.backend_pdf import PdfPages
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

source_file = os.path.abspath(__file__)
froot = os.path.join(os.path.dirname(source_file), '..', '..')

import importlib.util
spec = importlib.util.spec_from_file_location('data_utils', \
                os.path.join(froot, 'scripts', 'data', 'data_utils.py'))
data_utils = importlib.util.module_from_spec(spec)
spec.loader.exec_module(data_utils)

import fit_utils

warnings.filterwarnings('ignore', 'invalid value')

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------
parser = argparse.ArgumentParser(\
    description='Run ANN model', \
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-m', '--month', \
                    help='Calendar month', \
                    type=int, default=1)
parser.add_argument('-xv', '--xvconfig', \
                    help='Cross validation config', \
                    type=str, default='split')
parser.add_argument('-rf', '--rainfall_forecasts', \
                    help='Use rainfall forecasts as predictor', \
                    action='store_true', default=False)
parser.add_argument('-lr', '--linear_regression', \
                    help='Linear regression model', \
                    action='store_true', default=False)
parser.add_argument('-lo', '--loss_func', \
                    help='Loss function', \
                    type=str, default='lossmse', \
                    choices=['lossmse', 'lossbayes'])
parser.add_argument('-tr', '--training_dataset', \
                    help='Volume of training data', \
                    type=str, default='full', \
                    choices=['full', 'monthly'])
parser.add_argument('-ne', '--nens', \
                  help='Number of ensembles.', \
                  type=int, default=1000)
parser.add_argument('-nb', '--no_bjp', \
                    help='Do not show BJP data', \
                    action='store_true', default=False)
parser.add_argument('-as', '--all_sites', \
                    help='Plot all sites (otherwise restrict to bjp sites)',\
                    action='store_true', default=False)
parser.add_argument('-ps', '--pattern_search', \
                    help='Restrict sites to pattern search', \
                    type=str, default='.*')
args = parser.parse_args()
month = args.month
xvconfig = args.xvconfig
rainfall_forecasts = args.rainfall_forecasts
linear_regression = args.linear_regression
loss_func = args.loss_func
training_dataset = args.training_dataset
nens = args.nens
no_bjp = args.no_bjp
all_sites = args.all_sites
pattern_search = args.pattern_search

# General config
nepochs, learning_rate, early_stopping_patience, \
    trans, zero_thresh, data_version, \
    sites_xlim, sites_ylim, year_start, year_end, \
    data_patterns, days_tol, leave_out = \
                        data_utils.get_config(rainfall_forecasts)

# Plotted ensemble quantiles:
ens_qq = [5, 25, 50, 75, 95]
ens_qq_imed = 2

obs_color = 'red'
sim_color = 'steelblue'
bjp_color = 'green'

model_name = fit_utils.get_model_name(month, rainfall_forecasts, \
                        linear_regression, loss_func, training_dataset)
#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
fout = os.path.join(froot, 'outputs', 'fit')

flogs = os.path.join(froot, 'logs')
os.makedirs(flogs, exist_ok=True)

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
flog = os.path.join(flogs,  f'predict_{model_name}_XV{xvconfig}.log')
LOGGER = data_utils.get_logger(basename, flog)
data_utils.log_args(LOGGER, args)

#----------------------------------------------------------------------
# Get data
#----------------------------------------------------------------------
LOGGER.info('Loading data')
sites = data_utils.load_sites(data_version)
sites, pattern_sites = data_utils.filter_sites(sites, sites_xlim, sites_ylim)
nsites = len(sites)

data = data_utils.load_data(data_version)
data = data_utils.filter_data(data, year_start, year_end, pattern_sites)

LOGGER.info(f'Define training and validation data for scheme {xvconfig}')

# Use days_tol = 0 to keep values within a single month
idx_dict = data_utils.get_xv_indices(xvconfig, month, data.index,\
                                    training_dataset, \
                                    days_tol=0, \
                                    leave_out=leave_out)

#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------
Zobs = []
Zpred_ens = []
dates_pred = []

for xvkey, idxs in idx_dict.items():
    LOGGER.info(f'XV {xvkey}')

    # Create model directories
    model_path = os.path.join(fout, model_name, \
                   f'XV{xvkey}', 'deterministic')
    os.makedirs(model_path, exist_ok=True)

    prob_model_path = os.path.join(model_path, '..', 'probabilistic')
    os.makedirs(prob_model_path, exist_ok=True)

    # Extract training and validation data
    extract = fit_utils.get_WXZ(data, idxs, data_patterns, trans)
    input_vkeys = ['Wclim', 'Wrain', 'Wrunoff']
    xtest = [extract[vkey]['test']['raw'] for vkey in input_vkeys]
    ztest = extract['Z']['test']['raw']
    zcols = extract['Z']['test']['cols']

    # prediction dates
    idx_test = idxs['test']
    idx_pred = data.index[idx_test].day == 1

    d_pred = data.index[idx_test][idx_pred]

    LOGGER.info(f'XV{xvkey} - Loading probabilistic model')
    saved_model_prob = tf.saved_model.load(prob_model_path)

    LOGGER.info(f'XV{xvkey} - Run ensemble simulations')
    Zens = []
    for iens in range(nens):
        Z = saved_model_prob(xtest, training=False)
        Zens.append(Z.numpy()[idx_pred, :])

    Zens = np.array(Zens).T

    # Store
    Zpred_ens.append(Zens)
    Zobs.append(ztest[idx_pred, :].T)
    dates_pred.append(d_pred)

Zobs = np.array(Zobs).squeeze()
Zpred_ens = np.array(Zpred_ens).squeeze()
dates_pred = np.array(dates_pred).squeeze()

if xvconfig == 'years':
    Zpred_ens = np.moveaxis(Zpred_ens, 1, 0)
    Zobs = Zobs.T

fpdf = os.path.join(prob_model_path, '..', '..', \
            f'pred_{model_name}_XV{xvconfig}.pdf')
with PdfPages(fpdf) as pdf:
    iplot = 0
    for isite, cn in enumerate(zcols):
        siteid = re.sub('_.*', '', re.sub('.*ff_', '', cn))

        if not re.search(pattern_search, siteid):
            continue

        # Load bjp data and skip if needed
        bjp = data_utils.load_bjp2_hindcast(siteid)
        if not all_sites and bjp is None:
            continue

        LOGGER.info(f'Plotting {siteid} - plot #{iplot+1}')
        iplot += 1

        # Get station data
        #sim_det = Zpred_det[idx_plot_pred, isite]
        sim = Zpred_ens[isite, :, :]
        obs = Zobs[isite, :]
        obs[obs<0] = np.nan

        # Ensemble quantiles
        sim_qq = np.nanpercentile(sim, ens_qq, axis=1).T

        # Metrics
        rsp, nse, maes = fit_utils.perf_metrics(obs, \
                                    sim_qq[:, ens_qq_imed])

        # get bjp if available
        if not bjp is None:
            idx = bjp.index.isin(dates_pred)
            bjpsel = bjp.loc[idx, :]
            idx = [d for d in dates_pred if d not in bjpsel.index]
            tmp = pd.DataFrame(columns=bjpsel.columns, index=idx)
            bjpsel = pd.concat([bjpsel, tmp]).sort_index()
            bjp_med = bjpsel.filter(regex='^ens').median(axis=1)
            has_bjp = True

        # Plot
        plt.close('all')
        fig = plt.figure()
        gs = GridSpec(ncols=2, nrows=2)

        # Scatter plot
        ax = plt.subplot(gs[0, 0])

        # Plot bjp
        if not no_bjp and has_bjp:
            ax.plot(bjp_med.values, obs, 'o', color=bjp_color, \
                        label='BJP median')

        # Plot ML scatters
        sim_med = sim_qq[:, ens_qq_imed]
        ax.plot(sim_med, obs, 'o', alpha=0.8, \
                            label='ML median', color=sim_color)
        # Plot error bars
        for ipair, (s, o) in enumerate(zip(sim_qq, obs)):
            lab = None if ipair>0 else f'ML CI {ens_qq[1]}-{ens_qq[-2]}%'
            ax.plot([s[1], s[-2]], [o]*2, '-', color=sim_color, \
                                            lw=2, label=lab)

        ax.set_xlabel('Model Prediction [mm/m]')
        ax.set_ylabel('Obs [mm/m]')
        forward = lambda x: np.sqrt(x)
        inverse = lambda x: x*x
        ax.set_xscale('function', functions=(forward, inverse))
        ax.set_yscale('function', functions=(forward, inverse))
        txt = f'NSE = {nse:0.2f}\nR2s = {rsp:0.2f}\nMAES={maes:0.2f}'
        ax.text(0.02, 0.98, txt, transform=ax.transAxes, \
                    va='top', ha='left')

        (x0, x1), (y0, y1) = ax.get_xlim(), ax.get_ylim()
        ax.plot([x0, x1], [x0, x1], 'k-', lw=0.9)
        ax.set_xlim([x0, x1])
        ax.set_ylim([y0, y1])
        ax.legend(loc=4)

        # Ts plots
        ax = plt.subplot(gs[1, :])
        x = np.arange(1, 1+obs.shape[0])
        ax.plot(x, obs, 'o', lw=1.5, markersize=8, \
                        label='obs', color=obs_color)

        # ML ensembles
        props = {'color': sim_color, 'lw': 1.5}
        dx = 0 if no_bjp else 0.2
        ax.boxplot(sim.T, showfliers=False, whis=[5, 95], \
                    boxprops=props, \
                    capprops=props, \
                    whiskerprops=props, \
                    medianprops=props, \
                    positions=x-dx, widths=0.5-dx)
        ax.plot([], [], color=sim_color, lw=1.5, label='ML ens')

        # ML median
        ax.plot(x-dx, sim_med, 'o', label='ML median', color=sim_color)

        # BJP data
        if not no_bjp and has_bjp:
            ax.plot(x, bjpsel.obs.values, 'o', lw=1.5, markersize=6, \
                        label='BJP obs', alpha=0.2, color=obs_color)

            ax.plot(x+dx, bjp_med.values, 'o', \
                            label='BJP median', color=bjp_color)
            # Draw boxplot
            y = bjpsel.filter(regex='^ens').values.T
            props = {'color': bjp_color, 'lw': 1.5}
            bp = ax.boxplot(y, showfliers=False, whis=[5, 95], \
                    boxprops=props, \
                    capprops=props, \
                    whiskerprops=props, \
                    medianprops=props, \
                    positions=x+dx, widths=0.5-dx)

        ax.set_yscale('function', functions=(forward, inverse))
        ax.legend()
        ax.set_xticks(x)
        ax.set_xticklabels([d.year for d in pd.to_datetime(dates_pred)])
        y0, y1 = ax.get_ylim()
        ax.set_ylim((0, y1))
        ax.set_ylabel('Monthly runoff [mm/m]')

        # PIT plot
        pit = np.sum(sim-obs[:, None]<0, axis=1)/nens
        pit = np.sort(pit[~np.isnan(obs)])
        npit = len(pit)
        ppos = (np.arange(1,1+npit)-0.3)/(len(pit)+0.4)

        ax = plt.subplot(gs[0, 1])
        ax.plot(pit, ppos, '-o', color=sim_color, label='ML')

        if not no_bjp and has_bjp:
            y = bjpsel.filter(regex='^ens').values
            pit = np.sum(y-obs[:, None]<0, axis=1)/y.shape[1]
            pit = np.sort(pit[~np.isnan(obs) & ~np.isnan(bjp_med.values)])
            ppos = (np.arange(1,1+len(pit))-0.3)/(len(pit)+0.4)
            ax.plot(pit, ppos, '-o', color=bjp_color, label='BJP')

        ax.plot([0, 1], [0, 1], 'k-', lw=0.8)
        alpha = 0.05
        Dn = kolmogi(1-alpha)/math.sqrt(npit)
        ax.plot([0, 1], [Dn, 1+Dn], 'k--', lw=0.8, label='KS 5% CI')
        ax.plot([0, 1], [-Dn, 1-Dn], 'k--', lw=0.8)
        ax.set_xlim((0, 1))
        ax.set_ylim((0, 1))

        ax.set_title('PIT plot')
        ax.set_xlabel('PIT value [-]')
        ax.set_ylabel('ECDF [-]')
        ax.legend(loc=4)

        fig.set_size_inches((11, 9))
        gs.tight_layout(fig, rect=[0, 0, 1, 0.95])
        name = sites.loc[int(siteid), 'name']
        title = f'{siteid} {name}\nM{month:02d} XV{xvconfig}'
        fig.suptitle(title)
        pdf.savefig(fig)

LOGGER.info('Process completed')

