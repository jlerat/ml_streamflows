#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2021-01-30 Sat 03:00 PM
## Comment : Run ANN predict
##
## ------------------------------
import sys, os, re, json, math
import logging
import time, argparse
import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow import keras

from gridverif import gridmetrics

source_file = os.path.abspath(__file__)
froot = os.path.join(os.path.dirname(source_file), '..', '..')

import importlib.util
spec = importlib.util.spec_from_file_location('data_utils', \
                os.path.join(froot, 'scripts', 'data', 'data_utils.py'))
data_utils = importlib.util.module_from_spec(spec)
spec.loader.exec_module(data_utils)

import fit_utils

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------

# General config
nepochs, learning_rate, early_stopping_patience, \
    lam_trans, eps_trans, zero_thresh, data_version, \
    sites_xlim, sites_ylim, year_start, year_end, \
    pattern_por_rain_fcst, pattern_por_rain_obs, \
    pattern_por_runoff, pattern_pand, days_tol, leave_out = \
                                            data_utils.get_config()

#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
fout = os.path.join(froot, 'data', 'bjp2_hindcasts')

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
LOGGER = data_utils.get_logger(basename)

#----------------------------------------------------------------------
# Get data
#----------------------------------------------------------------------
LOGGER.info('Loading data')
sites = data_utils.load_sites(data_version)
sites, pattern_sites = data_utils.filter_sites(sites, sites_xlim, sites_ylim)
nsites = len(sites)

data = data_utils.load_data(data_version)
data = data_utils.filter_data(data, year_start, year_end, pattern_sites)

LOGGER.info('Loading BJP data')
bjp = {}
has_bjp = np.zeros(nsites).astype(bool)
for isite, (siteid, row) in enumerate(sites.iterrows()):
    if isite%5 == 0:
        LOGGER.info(f'.. {isite+1}/{nsites}')

    bjp[siteid] = data_utils.load_bjp2_hindcast(siteid)
    if not bjp[siteid] is None:
        has_bjp[isite] = True

#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------
scores = []

def store_score(values, has_bjp, month, name, xvconfig):
    s = pd.Series(np.nan, index=sites.index)
    s.loc[has_bjp] = values
    s.loc['month'] = month
    s.loc['score'] = name
    s.loc['xvconfig'] = xvconfig
    return s

for month in range(1, 13):
    LOGGER.info(f'Select data for month {month}')
    # Data indexes
    idx_tests = {}
    for xvconfig in ['years', 'split']:
        _, _, itests = data_utils.get_xv_indices(\
                                        xvconfig, month, data.index, \
                                        days_tol=0, \
                                        leave_out=leave_out)
        idx_test = data.index.month == 0
        for xvkey, itest in itests.items():
            idx_test |= itest

        # prediction dates
        dt = data.index[idx_test]
        idx_pred = (dt.day == 1) & (dt.year>=1980) & (dt.year <= 2008)
        dates_pred = data.index[idx_test][idx_pred]

        # Extract BJP data
        Bpred_ens = []
        Bobs = []
        for isite, (siteid, row) in enumerate(sites.iterrows()):
            if bjp[siteid] is None:
                continue

            # Filter prediction dates
            bjpmonth = bjp[siteid].loc[dates_pred, :]
            assert len(bjpmonth) == len(dates_pred)

            # Store data
            Bobs.append(bjpmonth.obs.values)
            Bpred_ens.append(bjpmonth.filter(regex='^ens', axis=1).values)

        Bobs = np.array(Bobs)
        Bpred_ens = np.array(Bpred_ens)

        LOGGER.info(f'Compute metrics for month {month}')
        crps_s, alpha, bias, qi = fit_utils.verif_scores(Bobs, Bpred_ens, \
                                            timedim=1, ensdim=2)
        scores.append(store_score(crps_s, has_bjp, month, 'crps_ss', xvconfig))
        scores.append(store_score(alpha, has_bjp, month, 'alpha', xvconfig))
        scores.append(store_score(bias, has_bjp, month, 'bias', xvconfig))
        scores.append(store_score(qi, has_bjp, month, 'qi', xvconfig))

# Write to disk
scores = pd.DataFrame(scores)
fs = os.path.join(fout, 'bjp2_hindcast_verif.csv')
scores.to_csv(fs, index=False, float_format='%0.3f')

LOGGER.info('Process completed')

