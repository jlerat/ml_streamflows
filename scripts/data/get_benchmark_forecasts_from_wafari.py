#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2021-02-10 Wed 12:34 PM
## Comment : Parse XV results from wafaridev
##
## ------------------------------
import sys, os, re, json, math
import logging
import argparse
from dateutil.relativedelta import relativedelta as delta

import numpy as np
import pandas as pd
import h5py

from datetime import datetime
from dateutil.relativedelta import relativedelta as delta

import requests

import data_utils

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------
#parser = argparse.ArgumentParser(\
#    description='A script', \
#    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
#
#parser.add_argument('-v', '--version', \
#                    help='Version number', \
#                    type=str, required=True)
#args = parser.parse_args()
#
#version = args.version

bjp_url = 'http://wafaridev.bom.gov.au/data/shared/ssf/wafari_ops_ble/wafari'

#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
source_file = os.path.abspath(__file__)

froot = os.path.join(os.path.dirname(source_file), '..', '..')
fdata = os.path.join(froot, 'data', 'bjp2_hindcasts')
fwafari = os.path.join(fdata, 'wafari_data')
os.makedirs(fwafari, exist_ok=True)

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
LOGGER = logging.getLogger(basename)
LOGGER.setLevel('INFO')
LOGGER.handlers = []
fmt = '%(asctime)s | %(name)s | %(levelname)s | %(message)s'
ft = logging.Formatter(fmt)
fh = logging.StreamHandler(sys.stdout)
fh.setFormatter(ft)
LOGGER.addHandler(fh)

#----------------------------------------------------------------------
# Get data
#----------------------------------------------------------------------

# Get bjp config
fj = os.path.join(fdata, 'bjp_sites_wafari.csv')
if not os.path.exists(fj):
    jurl = f'{bjp_url}/exp_report.json'
    with requests.get(jurl) as req:
        config_js = req.json()['project']

    # Convert to dataframe
    config = []
    for drainage, basins in config_js.items():
        for basin, catchments in basins.items():
            for catchment, sites in catchments.items():
                dd = {'drainage': drainage, \
                    'basin': basin, \
                    'catchment': catchment, \
                    'siteid': list(sites.keys())[0]}
                config.append(dd)

    bjp_sites = pd.DataFrame(config).set_index('siteid')
    bjp_sites.to_csv(fj, index=True)
else:
    bjp_sites = pd.read_csv(fj, index_col='siteid')

#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------

for siteid, row in bjp_sites.iterrows():
    LOGGER.info(f'Downloading data for {siteid}')

    # Download XV file
    urlh5 = f'{bjp_url}/output/bjp2/{row.basin}/{row.catchment}/'+\
            'out/monthly_xvalidate.hdf5'
    fh5 = os.path.join(fwafari, 'monthly_xvalidate.hdf5')
    req = requests.get(urlh5)
    with open(fh5, 'wb') as fo:
         fo.write(req.content)

    # Process
    with h5py.File(fh5, 'r') as h5:
        hind = []
        for month in range(1, 13):
            key = f'data/forecast/M{month:02d}D01/{siteid}.STREAMFLOW.P1M'
            obs = h5[key+'/observation'][:]
            ens = h5[key+'/simulation'][:]
            years = h5[key+'/year'][:]

            dates = pd.DatetimeIndex([datetime(y, month, 1) for y in years])
            cols = [f'ens{i:04d}' for i in range(ens.shape[1])]
            df = pd.DataFrame(ens, columns=cols, index=dates)
            df.loc[:, 'obs'] = obs
            hind.append(df)

        hind = pd.concat(hind)
        fh = os.path.join(fwafari, f'bjp2_hindcast_{siteid}.csv')
        hind.to_csv(fh, float_format='%0.3f')

    os.remove(fh5)

LOGGER.info('Process completed')

