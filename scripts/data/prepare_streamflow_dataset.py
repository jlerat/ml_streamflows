#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2020-02-19 16:26:43.128217
## Comment : Prepare daily flow data set with predictors
##
## ------------------------------
import sys, os, re, json, math
import argparse
from dateutil.relativedelta import relativedelta as delta

import numpy as np
import pandas as pd

from datetime import datetime
from dateutil.relativedelta import relativedelta as delta

from hydrodiy.io import csv, iutils
from hydrodiy.data import hyclimind

import datasets

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------
parser = argparse.ArgumentParser(\
    description='A script', \
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-v', '--version', \
                    help='Version number', \
                    type=str, required=True)
args = parser.parse_args()

version = args.version

start = datetime(1970, 1, 1)
end = datetime(2019, 7, 31)

climinds = ['nino34', 'nao', 'pdo', 'soi', 'iod']

# Excluded sites
excluded = ['410745', '410751']

#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
source_file = os.path.abspath(__file__)

froot = os.path.join(os.path.dirname(source_file), '..', '..')
fdata = os.path.join(froot, 'data')
os.makedirs(fdata, exist_ok=True)

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
LOGGER = iutils.get_logger(basename)

#----------------------------------------------------------------------
# Get data
#----------------------------------------------------------------------
dset = datasets.Dataset('OZDATA', '1.0')
sites = dset.get_sites()

# exclude sites
idx = ~sites.index.isin(excluded)
sites = sites.loc[idx, :]

# Climate indices
climinds = {}
tmp, _ = dset.get('410734', 'rainfall_runoff', 'D')
tmp = tmp.loc[start:end, :]
dates = tmp.index[tmp.index.day%2==1]
for ind_name in ['nino34', 'nino12', 'nino4', 'soi']:
    ind_val, _ = hyclimind.get_data(ind_name)
    ind_val = ind_val.loc[start:end]
    ind_val.name = ind_name

    # Set proper dates
    ind_val2 = ind_val.reindex(dates).fillna(method='pad').fillna(-999)
    climinds[f'por_clim_{ind_name}'] = ind_val2

climinds = pd.DataFrame(climinds)

#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------

#predictors = {'nino34': nino34}
#predictands = {}
#sites_selected = []
alldata = []
sites_selected = {}

def rolling(se, shift, window, minper):
    return se.shift(shift).rolling(window=f'{window}D', \
                min_periods=minper).mean()*window


for i, (siteid, row) in enumerate(sites.iterrows()):
    LOGGER.info('dealing with {0} ({1}/{2})'.format( \
        siteid, i, len(sites)))

    daily, _ = dset.get(siteid, 'rainfall_runoff', 'D')
    daily = daily.loc[start:end, :]

    # Aggregate
    df = {}
    for varname in ['runoff', 'rain']:
        se = daily.loc[:, f'{varname}[mm/d]']

        # Next 30, 20 and 10 days
        prefix = 'pand' if varname == 'runoff' else 'por'
        df[f'{prefix}_{varname}_{siteid}_n30d'] = rolling(se, -29, 30, 25)
        df[f'{prefix}_{varname}_{siteid}_n20d'] = rolling(se, -19, 20, 17)
        df[f'{prefix}_{varname}_{siteid}_n10d'] = rolling(se, -9, 10, 7)

        # Last 30, 20, 10 and 5 days
        df[f'por_{varname}_{siteid}_p30d'] = rolling(se, 1, 30, 25)
        df[f'por_{varname}_{siteid}_p20d'] = rolling(se, 1, 20, 17)
        df[f'por_{varname}_{siteid}_p10d'] = rolling(se, 1, 10, 7)
        df[f'por_{varname}_{siteid}_p05d'] = rolling(se, 1, 5, 4)

    df = pd.DataFrame(df)

    # Check
    se = daily.loc[:, f'runoff[mm/d]']
    se[se<0] = np.nan
    p20d = rolling(se, 1, 20, 20)
    n30d = rolling(se, -29, 30, 30)
    days = pd.date_range('2000-01-01', '2010-12-31', freq='50D')
    for day in days:
        v1 = n30d[day]
        v2 = np.sum(se.loc[day:day+delta(days=29)].values)
        assert np.isclose(v1, v2, equal_nan=True)

        v1 = p20d[day]
        v2 = np.sum(se.loc[day-delta(days=20):day-delta(days=1)].values)
        assert np.isclose(v1, v2, equal_nan=True)


    # Select data for 1/10/20 of the month
    df = df.loc[df.index.day%2==1]

    # Store
    df = df.fillna(-999)

    # Check data is there
    cn = f'pand_runoff_{siteid}_n30d'
    pmiss = df.groupby(df.index.month).apply(lambda x: (x<0).sum()/len(df))
    pmiss_max = pmiss.max().max()
    if pmiss_max > 0.05:
        LOGGER.error(f'Max missing = {pmiss_max*100:0.1f}%')
        continue
    else:
        LOGGER.info(f'Max missing = {pmiss_max*100:0.1f}% > OK. '+\
                        f'total={len(sites_selected)}')

    # Add climate indices for the first dataset
    if i == 0:
        df = pd.concat([climinds, df], axis=1)

    # Store
    alldata.append(df)
    sites_selected[siteid] = row

# Store
alldata = pd.concat(alldata, axis=1)
fd = os.path.join(fdata, f'streamflow_data_version{version}.csv')
comments = {
    'columns': 'por=predictors, pand=predictands, n30d=next 30 days,'+\
                'p30d=previous 30 days, p20d=previous 20 days, ...', \
    'units': 'Rainfall and runoff in mm.', \
    'source': 'Streamflow data from Water Data Online, Rainfall '+\
                'data from AWAP (Bureau of Meteorology)'
}
csv.write_csv(alldata, fd, comments, \
                    source_file, write_index=True, compress=False, \
                    author='Julien Lerat, Bureau of Meteorology', \
                    float_format='%0.3f')

sites_selected =  pd.DataFrame(sites_selected).T
sites_selected.index.name = 'siteid'
fs = os.path.join(fdata, f'streamflow_sites_version{version}.csv')
csv.write_csv(sites_selected, fs, 'Sites for streamflow forecasting', \
                    source_file, write_index=True, compress=False, \
                    author='Julien Lerat, Bureau of Meteorology')

LOGGER.info('Process completed')

