#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2021-02-09 Tue 09:05 AM
## Comment : plot site map
##
## ------------------------------
import sys, os, re, json, math
import logging
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle as Rect

source_file = os.path.abspath(__file__)
froot = os.path.join(os.path.dirname(source_file), '..', '..')

import importlib.util
spec = importlib.util.spec_from_file_location('data_utils', \
                os.path.join(froot, 'scripts', 'data', 'data_utils.py'))
data_utils = importlib.util.module_from_spec(spec)
spec.loader.exec_module(data_utils)

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------
# General config
nepochs, learning_rate, early_stopping_patience, \
    lam_trans, eps_trans, zero_thresh, data_version, \
    sites_xlim, sites_ylim, year_start, year_end, \
    pattern_por_rain_fcst, pattern_por_rain_obs, \
    pattern_por_runoff, pattern_pand, month_tol, leave_out = \
                                            data_utils.get_config()

# color
color_train = 'tab:blue'
color_valid = 'tab:orange'
color_test = 'tab:red'

#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
fimg = os.path.join(froot, 'images')
os.makedirs(fimg, exist_ok=True)

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
LOGGER = data_utils.get_logger(basename)

#----------------------------------------------------------------------
# Get data
#----------------------------------------------------------------------
LOGGER.info('Loading data')
sites = data_utils.load_sites(data_version)
sites, pattern_sites = data_utils.filter_sites(sites, sites_xlim, sites_ylim)
nsites = len(sites)

data = data_utils.load_data(data_version)
data = data_utils.filter_data(data, year_start, year_end, pattern_sites)

#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------
LOGGER.info('Plot xvperiods')

month = 1

# Year split
xvconfig = 'split'
idx_train, idx_valid, idx_test = data_utils.get_xv_indices(\
                                    xvconfig, month, data.index, \
                                    month_tol=month_tol, \
                                    leave_out=5)
ytrain = np.unique(data.index[idx_train].year)
yvalid = np.unique(data.index[idx_valid].year)
ytest = np.unique(data.index[idx_test].year)

yall = np.sort(np.concatenate([ytrain, yvalid, ytest]))
plt.close('all')
fig, ax = plt.subplots()
ax.plot([yall[0], yall[-1]], [0, 1], color='none')
dy = 0.8
for i, y in enumerate(yall):
    if y in ytrain:
        color = color_train
    elif y in yvalid:
        color = color_valid
    else:
        color = color_test
    yr = Rect((y-dy/2, 0), dy, 1, edgecolor='none', \
                    facecolor=color, linewidth=1)
    ax.add_patch(yr)

yt = yall[::3]
ax.set_xticks(yt)
ax.set_xticklabels(yt, fontsize=15, fontweight='bold')
ax.set_yticks([])
for s in ['top', 'right', 'bottom', 'left']:
    ax.spines[s].set_visible(False)
ax.plot([], [], lw=10, color=color_train, label='Train')
ax.plot([], [], lw=10, color=color_valid, label='Valid')
ax.plot([], [], lw=10, color=color_test, label='Test')
ax.legend(loc='lower left', bbox_to_anchor= (0.0, 1.01), ncol=3, \
            borderaxespad=0, frameon=False, \
            prop={'weight':'bold', 'size':16})
ax.set_xlim([yall[0]-dy/2-0.1, yall[-1]+dy/2+0.1])

fig.set_size_inches((20, 2))
fig.tight_layout(rect=[0, 0, 1, 0.95])
fp = os.path.join(fimg, 'xvper_split.png')
fig.savefig(fp)



LOGGER.info('Process completed')

