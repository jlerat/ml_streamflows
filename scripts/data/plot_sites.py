#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2021-02-09 Tue 09:05 AM
## Comment : plot site map
##
## ------------------------------
import sys, os, re, json, math
import logging
import argparse
import pandas as pd

import io
from PIL import Image

import folium
from folium.plugins import MiniMap

source_file = os.path.abspath(__file__)
froot = os.path.join(os.path.dirname(source_file), '..', '..')

import importlib.util
spec = importlib.util.spec_from_file_location('data_utils', \
                os.path.join(froot, 'scripts', 'data', 'data_utils.py'))
data_utils = importlib.util.module_from_spec(spec)
spec.loader.exec_module(data_utils)

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------
parser = argparse.ArgumentParser(\
    description='plot site map', \
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-v', '--data_version', \
                  help='Dataset version', \
                  type=int, default=5)
parser.add_argument('-p', '--to_png', \
                  help='Save image to png', \
                  action='store_true', default=False)
args = parser.parse_args()
data_version = args.data_version
to_png = args.to_png

# General config
nepochs, learning_rate, early_stopping_patience, \
    lam_trans, eps_trans, zero_thresh, _, \
    sites_xlim, sites_ylim, year_start, year_end, \
    pattern_por_rain_fcst, pattern_por_rain_obs, \
    pattern_por_runoff, pattern_pand, month_tol, leave_out = \
                                            data_utils.get_config()
#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
fimg = os.path.join(froot, 'images')
os.makedirs(fimg, exist_ok=True)

basename = re.sub('\\.py.*', '', os.path.basename(source_file))
LOGGER = logging.getLogger(basename)
LOGGER.setLevel('INFO')
LOGGER.handlers = []
fmt = '%(asctime)s | %(name)s | %(levelname)s | %(message)s'
ft = logging.Formatter(fmt)
fh = logging.StreamHandler(sys.stdout)
fh.setFormatter(ft)
LOGGER.addHandler(fh)

#----------------------------------------------------------------------
# Get data
#----------------------------------------------------------------------
LOGGER.info('Loading data')
sites = data_utils.load_sites(data_version)
sites, pattern_sites = data_utils.filter_sites(sites, sites_xlim, \
                                                    sites_ylim)
#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------
LOGGER.info('Plot map')
x, y = sites.lon_centroid_dem.mean(), sites.lat_centroid_dem.mean()
m = folium.Map(location=[y, x], zoom_start=6)

minimap = MiniMap()
m.add_child(minimap)

# Add sites
for siteid, row in sites.iterrows():
    x = row.lon_outlet
    y = row.lat_outlet
    name = row.loc['name']
    tooltip = str(siteid)
    popup=f'<i>{siteid} {name}</i>'
    folium.Marker(
        [y, x], \
        popup=popup, \
        tooltip=tooltip #, \
        #icon=folium.Icon(color='red', icon='map-pin', prefix='fa')
    ).add_to(m)

    #folium.Circle(
    #    radius=5000, \
    #    location=[y, x], \
    #    popup=popup, \
    #    color="crimson", \
    #    fill=False, \
    #).add_to(m)

LOGGER.info('Export to html')
fh = os.path.join(fimg, f'site_location_version{data_version}.html')
m.save(fh)

LOGGER.info('Export to png')
if to_png:
    img_data = m._to_png(5)
    img = Image.open(io.BytesIO(img_data))
    fp = re.sub('html$', 'png', fh)
    img.save(fp)

LOGGER.info('Process completed')

