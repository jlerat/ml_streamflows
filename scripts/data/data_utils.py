#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os, json, re, sys
import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta as delta

import pandas as pd
import numpy as np
from scipy.stats import spearmanr

FROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..')
FDATA = os.path.join(FROOT, 'data')

# BJP XV years
BJP_YEARS = np.arange(1980, 2009)

def get_logger(basename, flog=None):
    LOGGER = logging.getLogger(basename)
    LOGGER.setLevel('INFO')
    LOGGER.handlers = []
    fmt = '%(asctime)s | %(name)s | %(levelname)s | %(message)s'
    ft = logging.Formatter(fmt)
    if flog is None:
        fh = logging.StreamHandler(sys.stdout)
    else:
        try:
            os.remove(fh)
        except:
            pass
        fh = logging.FileHandler(flog)

    fh.setFormatter(ft)
    LOGGER.addHandler(fh)

    return LOGGER

def log_args(LOGGER, args):
    LOGGER.info('')
    LOGGER.info('------------')
    LOGGER.info('Script run with the following arguments:')
    for arg in vars(args):
        LOGGER.info(f'{arg} : {getattr(args, arg)}')
    LOGGER.info('------------')
    LOGGER.info('')


def get_config(rainfall_forecasts, fjson=None):
    if fjson is None:
        fjson = os.path.join(FROOT, 'scripts', 'fit', 'fit_config.json')

    with open(fjson, 'r') as fo:
        config = json.load(fo)

    # Training config
    nepochs = int(config['nepochs'])
    learning_rate = float(config['learning_rate'])
    early_stopping_patience = int(config['early_stopping_patience'])
    days_tol = int(config['days_tol'])
    leave_out = int(config['leave_out'])

    # Power transform parameters
    lam_trans = float(config['lam_trans'])
    eps_trans = float(config['eps_trans'])
    zero_thresh = float(config['zero_thresh'])

    # Data version (not used in Azure though which relies on an Azure dataset)
    data_version = int(config['data_version'])

    # Site selection box
    sites_xlim = [float(x) for x in config['sites_xlim']]
    sites_ylim = [float(y) for y in config['sites_ylim']]

    # Training years
    year_start = int(config['year_start'])
    year_end = int(config['year_end'])

    # Define rainfall predictors
    if rainfall_forecasts:
        pattern_por_rain = config['pattern_por_rain_fcst']
    else:
        pattern_por_rain = config['pattern_por_rain_obs']

    # Collect data patterns
    data_patterns = {
        'Wrain': pattern_por_rain, \
        'Wrunoff': config['pattern_por_runoff'], \
        'Wclim': config['pattern_por_clim'], \
        'Z': config['pattern_pand']
    }

    # Define transformation
    trans = {
        'Wrain': {'lam': 0.3, 'eps': 1e-3}, \
        'Wrunoff': {'lam': 0.3, 'eps': 1e-3}, \
        'Wclim': {'lam': None, 'eps': None}, \
        'Z': {'lam': 0.3, 'eps': 1e-3}
    }

    return nepochs, learning_rate, early_stopping_patience, \
            trans, zero_thresh, data_version, \
            sites_xlim, sites_ylim, year_start, year_end, \
            data_patterns, days_tol, leave_out


def load_sites(version, LOGGER=None):
    fs = os.path.join(FDATA, f'streamflow_sites_version{version}.csv')
    if os.path.exists(fs):
        sites = pd.read_csv(fs, index_col=0, parse_dates=True, skiprows=15)

    else:
        if not LOGGER is None:
            LOGGER.info('Loading data from Azure dataset')

        # azureml-core of version 1.0.72 or higher is required
        # azureml-dataprep[pandas] of version 1.1.34 or higher is required
        from azureml.core import Workspace, Dataset

        subscription_id = '298dde50-3721-42c6-8d26-5e1ad59d23d3'
        resource_group = 'bom'
        workspace_name = 'bom'

        workspace = Workspace(subscription_id, resource_group, workspace_name)

        dataset = Dataset.get_by_name(workspace, name='streamflow_sites')
        sites = dataset.to_pandas_dataframe().set_index('siteid')

    return sites


def load_data(version, LOGGER=None):
    fs = os.path.join(FDATA, f'streamflow_sites_version{version}.csv')
    if os.path.exists(fs):
        fd = os.path.join(FDATA, f'streamflow_data_version{version}.csv')
        data = pd.read_csv(fd, index_col=0, parse_dates=True, skiprows=17)
    else:
        if not LOGGER is None:
            LOGGER.info('Loading data from Azure dataset')

        # azureml-core of version 1.0.72 or higher is required
        # azureml-dataprep[pandas] of version 1.1.34 or higher is required
        from azureml.core import Workspace, Dataset

        subscription_id = '298dde50-3721-42c6-8d26-5e1ad59d23d3'
        resource_group = 'bom'
        workspace_name = 'bom'

        workspace = Workspace(subscription_id, resource_group, workspace_name)

        dataset = Dataset.get_by_name(workspace, name='streamflow_rainfall')
        data = dataset.to_pandas_dataframe()
        data.loc[:, 'Column2'] = pd.to_datetime(data.Column2)
        data = data.set_index('Column2')

    return data


def load_bjp2_hindcast(siteid):
    fh = os.path.join(FDATA, 'bjp2_hindcasts', 'wafari_data', \
                                    f'bjp2_hindcast_{siteid}.csv')
    if os.path.exists(fh):
        hindcast = pd.read_csv(fh, parse_dates=True, index_col=0)
        return hindcast
    else:
        return None


def filter_sites(sites, xlim, ylim):
    ''' Filter sites located in a lat/lon box '''
    idx = (sites.lon_centroid_dem > xlim[0]) \
            & (sites.lon_centroid_dem < xlim[1]) \
            & (sites.lat_centroid_dem > ylim[0]) \
            & (sites.lat_centroid_dem < ylim[1])
    pattern_sites = '|'.join([str(siteid) \
                    for siteid in sites.index[idx].tolist()])+ '.*'
    sites = sites.loc[idx, :]
    return sites, pattern_sites


def filter_data(data, year_start, year_end, pattern_sites):
    ''' Filter data between start and end years and selected sites
        data : pandas data frame containing time series data
        year_start: start
        year_end : end
        pattern_sites : regex pattern to filter sites
    '''
    data = data.loc[str(year_start):str(year_end), :].filter(\
                    regex=pattern_sites+'|clim', axis=1)

    return data


def get_xv_indices(xvconfig, month, dates, training_dataset, days_tol, leave_out=5):
    ''' Get indices defining train, test and valid periods
        xvconfig : XV configuration (split/full/a year)
        month : Calendar month
        dates : Date/time index used in dataset
        training_dataset : Type of training. full=all data, monthly=only first
                                                    of month
        days_tol : Tolerance on month selection
        leaveout : Leave out period if xvconfig is a particular year
    '''
    # Locate dates within months +/- days_tol
    idays = dates.month == 0
    for year in np.unique(dates.year):
        day1 = datetime(year, month, 1)
        start = day1-delta(days=days_tol+1)
        end = day1+delta(months=1)+delta(days=days_tol-1)
        idays |= (dates>=start) & (dates<=end)

    if training_dataset == 'monthly':
        idays &= dates.day == 1

    if xvconfig == 'years':
        idx_dict = {}

        for xvyear in BJP_YEARS:
            # Define training period out of xvyear..xvyear+leaveout
            xvper = np.arange(xvyear, xvyear+leave_out)
            idx = ~dates.year.isin(xvper) & idays
            idx_test = (dates.year == xvyear) & idays

            # Remove 1/3 of years from training and set them as valid
            y = np.unique(dates.year[idx])
            yvalid = y[::3]
            ytrain = list(set(y.tolist())-set(yvalid.tolist()))
            idx_train = dates.year.isin(ytrain) & idays
            idx_valid = dates.year.isin(yvalid) & idays

            # Store
            idx_dict[xvyear] = {
                'train': idx_train,
                'valid': idx_valid,
                'test':idx_test
            }

    elif xvconfig == 'full':
        # Train on full set
        idx_train = idays
        idx_valid = idays
        idx_test = idays

        # Store
        idx_dict = {
            'full' : {
                'train': idx_train,
                'valid': idx_valid,
                'test':idx_test
            }
        }

    elif xvconfig == 'split':
        # 50% train, 25% valid, 25% test
        years = np.unique(dates.year)
        ytrain = years[::2]
        y = years[1::2]
        yvalid = y[::2]
        ytest = y[1::2]

        idx_train = idays & dates.year.isin(ytrain)
        idx_valid = idays & dates.year.isin(yvalid)
        idx_test = idays & dates.year.isin(ytest)

        # Store
        idx_dict = {
            'split' : {
                'train': idx_train,
                'valid': idx_valid,
                'test':idx_test
            }
        }

    # Just to make sure there is no overlap
    for xvkey in idx_dict:
        idx_train = idx_dict[xvkey]['train']
        idx_test = idx_dict[xvkey]['test']
        idx_valid = idx_dict[xvkey]['valid']
        assert (idx_train*idx_valid*idx_test).sum() == 0

    return idx_dict


