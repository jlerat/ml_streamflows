#!/usr/bin/env python
# -*- coding: utf-8 -*-

## -- Script Meta Data --
## Author  : jlerat
## Created : 2021-02-09 Tue 12:19 PM
## Comment : Parse XV results from public BJP forecast site
##
## ------------------------------
import sys, os, re, json, math
import logging
import argparse
from dateutil.relativedelta import relativedelta as delta

import numpy as np
import pandas as pd
from bs4 import BeautifulSoup

from datetime import datetime
from dateutil.relativedelta import relativedelta as delta

import requests

import data_utils

#----------------------------------------------------------------------
# Config
#----------------------------------------------------------------------
#parser = argparse.ArgumentParser(\
#    description='A script', \
#    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
#
#parser.add_argument('-v', '--version', \
#                    help='Version number', \
#                    type=str, required=True)
#args = parser.parse_args()
#
#version = args.version

# General config
nepochs, learning_rate, early_stopping_patience, \
    lam_trans, eps_trans, zero_thresh, data_version, \
    sites_xlim, sites_ylim, year_start, year_end, \
    pattern_por_rain_fcst, pattern_por_rain_obs, \
    pattern_por_runoff, pattern_pand, month_tol, leave_out = \
                                            data_utils.get_config()

bjp_url = 'http://www.bom.gov.au/water/ssf/content'

#----------------------------------------------------------------------
# Folders
#----------------------------------------------------------------------
source_file = os.path.abspath(__file__)

froot = os.path.join(os.path.dirname(source_file), '..', '..')
fdata = os.path.join(froot, 'data', 'bjp2_hindcasts')
os.makedirs(fdata, exist_ok=True)


basename = re.sub('\\.py.*', '', os.path.basename(source_file))
LOGGER = logging.getLogger(basename)
LOGGER.setLevel('INFO')
LOGGER.handlers = []
fmt = '%(asctime)s | %(name)s | %(levelname)s | %(message)s'
ft = logging.Formatter(fmt)
fh = logging.StreamHandler(sys.stdout)
fh.setFormatter(ft)
LOGGER.addHandler(fh)

#----------------------------------------------------------------------
# Get data
#----------------------------------------------------------------------
sites = data_utils.load_sites(data_version)

# Get bjp config
fj = os.path.join(fdata, 'bjp_sites.csv')
if not os.path.exists(fj):
    jurl = f'{bjp_url}/site_config.json'
    with requests.get(jurl) as req:
        config_js = req.json()['leaf']['features']

    with open(fj, 'w') as fo:
        json.dump(config_js, fo, indent=4)

    # Convert to dataframe
    config = {}
    for cfg in config_js:
        dd = {k:v for k, v in cfg['properties'].items() \
                    if not k in ['nearby', 'months_not_modelled']}
        dd['lon'] = cfg['geometry']['coordinates'][0]
        dd['lat'] = cfg['geometry']['coordinates'][1]
        config[cfg['properties']['ID']] = dd

    bjp_sites = pd.DataFrame(config).T
    bjp_sites.to_csv(fj, index=True)

else:
    bjp_sites = pd.read_csv(fj, index_col=0)

#----------------------------------------------------------------------
# Process
#----------------------------------------------------------------------
nfound = 0
hindcasts = []

for siteid, row in sites.iterrows():
    LOGGER.info(f'Downloading data for {siteid} (nfound {nfound})')

    # Find corresponding BJP sites
    ibjp = bjp_sites.index == row.BJPid
    if ibjp.sum() == 1:
        info = bjp_sites.loc[ibjp, :].squeeze()
        nfound += 1

        for month in range(1, 13):
            xvurl = f'{bjp_url}/{info["drainage"]}/{info["basin"]}/'+\
                f'xv/{month:02d}/{info["ID"]}_XV_7_{month:02d}_C1M_summary.html'
            with requests.get(xvurl) as req:
                html = req.content
            soup = BeautifulSoup(html, 'lxml')
            table = soup.find_all('table')[0]

            df = {}
            irow = 0
            for hrow in table.find_all('tr'):
                icol = 0
                columns = hrow.find_all('td')
                dd = {}
                for column in columns:
                    txt = column.get_text()
                    vol_gl = float(txt) if txt != 'NA' else np.nan
                    depth_mm = vol_gl*1e3/info['area']
                    dd[icol] = depth_mm
                    icol += 1

                if irow == 1:
                    prefix = [''] + ['fcst']*5+['clim']*5+['obs']
                    headers = [p+'_'+h.get_text()+'[mm/m]' for p, h \
                                    in zip(prefix, hrow.find_all('th'))]
                    headers[-1] = re.sub('_', '', headers[-1])

                elif irow > 1:
                    year = int(hrow.find_all('th')[0].get_text())
                    dmonth = datetime(year, month, 1)
                    df[dmonth] = dd
                irow += 1

            df = pd.DataFrame(df).T
            df.columns = headers[1:]
            df.index.name = headers[0]
            df.loc[:, 'siteid'] = siteid
            hindcasts.append(df.reset_index())

    else:
        LOGGER.error(f'... no match in bjp site list for id {siteid}')

# Store
hindcasts = pd.concat(hindcasts, axis=0)
fh = os.path.join(fdata, 'bjp2_hindcast.csv')
hindcasts.to_csv(fh, float_format='%0.4f')


LOGGER.info('Process completed')

